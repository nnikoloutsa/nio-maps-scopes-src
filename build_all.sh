#!/bin/bash 

declare -a customer_cmds
idx=0
for customer_path in data/customers/*; do
	customer_name=$(basename "$customer_path")
	customer_cmds[$idx]="python locations_updater.py --customer=$customer_name"
	idx=$((idx + 1))
done

parallel ::: "${customer_cmds[@]}"

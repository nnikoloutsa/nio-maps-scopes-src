## Maps API

## Description

This repository manages the location information of nio: scopes, location hierarchies, geojson per location.

The customers cellmap.json, scopemap.json, scopes.json files, and the contents of maps_locations, maps_hierarchies tables in analytics db are stored/managed in this repo.

# Structure of Maps API repo (nio-maps-scopes)

```
├── README.md
├── artifacts
├── data
├── load_locations.sh
├── locations_updater.sh
├── locations_migrate.sh
├── release
└── src
```

# Prerequisites

Clone the repo from GitLab (note: the previous BitBucket based repo is obsolete. Delete it or rename it and clone the new one)



# Data directory

Here we keep the configuration for each customer's locations.

```
data
├── customers
│   ├── hkt
│   ├── hutchison
│   ├── hutchison_staging
│   ├── indosat
│   ├── ogkw
│   ├── ooredoo-mdv
│   ├── singtel
│   ├── singtel-fixed-bdk-mu1p
│   ├── smartfren
│   ├── telkom
│   └── telkomsel
└── global
    ├── country_information.json
    └── geojson
```

Global directory holds the names, codes etc for 150+ countries and their geojson data.

For a customer, the data we keep are:

```
data/customers/singtel
├── config
│   ├── cellmap.json
│   ├── scopemap.json
│   └── scopes.json
├── geojson
│   ├── district
│   ├── poi
│   └── site
```

(depending on what each customer gives us, we could have geojson info for more scopes).

The scopes.json file is the main configuration. It defines some meta data about the country (nation), the FULL scope hierarchy of the customer (in order), plus a list of named subsets.

Note: nation and cgi (cell) scopes are added by default at the top and bottom of "scopes".

```
    {
    "meta": {
      "nation": "Singapore",
      "default_coordinates": [6.1750, 106.8283],
      "root_is_point_scope": false
    },
    "scopes": [
      {
        "scope": "scope_3",
        "cli_key": "district",
        "cellmap_key": "district",
        "scope_name": "district",
        "geojson.deduce_polygon": false,
        "geojson.deduce_centroid": false,
        "point": true
      },
      {
        "scope": "site",
        "cli_key": "bts",
        "cellmap_key": "bts",
        "scope_name": "site",
        "point": true
      }
    ],
    "hierarchies": {
      "api": [
        {
          "scope_name": "district",
          "cluster": false
        },
        {
          "scope_name": "site",
          "cluster": false
        }
      ]
    }
  }
```

Here api is a named hierarchy -- a subset of the full scopes hierarchy. A customer can have multiple scope hierarchies, one for mapview (region, city, village, cell) for example, and another for trends4g (region, city, kabupaten, village, bts, cell).

The only file that needs updating from these due to customer changes is usually "cellmap.json".

We then run our scripts to generate the new locations list for the customer:

```
./locations_updater.sh --customer=singtel
```

This reads the scopes.json, cellmap.json, geojson, and scopemap.json for the customer, and prepares the JSON file that defines the API scope info (that's similar to the location.sqlite3 db we produced previously).

After running the script, the new locations info, scopes.json and scopemap.json will be placed in the artifacts directory on a folder named after the current datetime. A symlink named "latest" will also be updated to point to the latest results:

```
artifacts/
└── customers
    ├── hutchison
    │   ├── 20180220-161211
    │       ├── locations.json
    │       ├── scopes.json.json
    │       ├── 20180220-161211/scopesmap.json
```

The xxx-xxx-locations.json is a single JSON file, stamped with the datetime and customer name that contains the locations hierarchy + geojson info for that customer.

It's a set of entries like:

```
	    {
                "parent_id": 2603,
                "right": 41483,
                "name": "525-01-778-734657-15",
                "geojson": "{\"type\": \"Point\", \"coordinates\": [103.7168333, 1.350861111]}",
                "scope": "cgi",
                "left": 41482,
                "mcc": "",
                "id": 34768,
                "coordinates": "{\"lat\": 1.350861111, \"lon\": 103.7168333}",
                "parent_scope": "site"
            },
```

Plus a listing of the hierarchies in the customer:
```
    "hierarchies": {
        "api": [
            {
                "point": false,
                "geojson.simplification_level": "",
                "geojson.deduce_polygon": false,
                "cluster": false,
                "parent_scope_name": "overall",
                "scope_name": "nation",
                "order": 1,
                "geojson.deduce_centroid": false
            },
            {
                "cluster": false,
                "point": true,
                "parent_scope_name": "nation",
                "order": 2,
                "geojson.deduce_polygon": false,
                "cellmap_key": "district",
                "scope_name": "district",
                "scope": "scope_3",
                "cli_key": "district",
                "geojson.deduce_centroid": false
            },
            {
                "point": true,
                "parent_scope_name": "district",
                "order": 3,
                "cluster": false,
                "cellmap_key": "bts",
                "scope_name": "site",
                "scope": "site",
                "cli_key": "bts"
            },
            {
                "point": true,
                "geojson.simplification_level": "",
                "geojson.deduce_polygon": false,
                "cluster": false,
                "parent_scope_name": "site",
                "scope_name": "cgi",
                "order": 4,
                "geojson.deduce_centroid": false
            }
        ]
    },
```

We can now run a script to load the new locations to the customer's MRS:

```
./load_locations.sh <CUSTOMER_NAME> <SSH-able MRS HOSTNAME/IP>
```

eg:

```
./load_locations.sh singtel singtel-bdk-mu1p
```

This will compress and upload the locations file to the customer, and load it to their DB (table: analytics/maps_locations).

example of migrate locations from sqlite db to the new file structure:
```
python -B -u locations_migrate.py --customer=indosat --scope=all --sqlite_file=data/customers/indosat/location.sqlite
```

example of create locations from cellmap + the new file structure:
```
python -B -u locations_updater.py --customer=indosat
```

example of loading locations to customer db:
```
bash ./load_locations.sh singtel singtel-bdk-mu1p
```

-------

## Customer files

### analytics_id_groups.json

This file is used during RPM build time to define groups of key apps that get refferenced by the nlive_aggregations.json file.
The resulting RPM nlive_aggregations.json file contains the key app ids found in nalytics_id_groups.json.

It's also read by itself (at runtime) for enumeratons.

Apps must be listed with their app_analytics_id.

NOTE: for customers with *copy.txt*, the nlive_aggregations.json must contain the fully enumerated list of apps,
as the preprocessing of analytics_id_groups.json doesn't happen.

### nlive_aggregations.json

Defines which aggregations nlive will perform.

When it comes to location scopes, it should only refer to location scopes in hardcoded_nlive_abstracts or hardcoded_nlive_named (see scopes.json entry)

(But it can also mention all kind of other dimensions, e.g. sgsn etc)

NOTE: for customers with *copy.txt*, the nlive_aggregations.json must contain the fully enumerated list of apps,
as the preprocessing of analytics_id_groups.json doesn't happen.

### copy.txt

Customers with copy.txt are a special case. For them, the nio-maps-scopes preprocessing doesn't happen,
and a number of files in data/customers/xxx/config/ are copied AS IS to the RPM. E.g. the scopemap.json file
is not generated for them, but copied (and must be present in their data/customers/xxx/config directory).

NOTE: for customers with *copy.txt*, the nlive_aggregations.json must contain the fully enumerated list of apps,
as the preprocessing of analytics_id_groups.json doesn't happen.

### keyapps.json

DEPRECATED (useless in 652, and removed as of 653)

### cellmap.json

The full list of all customer base scope location (cgi's in telcos, could be something else in fixed customers),
with their details and the list of all parent scope locations each belongs to.

### eventlog.json.maps.local

Configures how nlive will produce historical logs for different scope_x scopes (filename of logfile, dimensions, rotation, etc).

### m2m_whitelist.txt

Machine to Machine, imsi range definition.
This subscriber's group is used at Roaming Analytics Business Managment workspace.

### iot_whitelist.txt

IoT, imsi ranges definition.
This subscriber's group is used at IoT workspace.

### migrations.json

Ignore. Devel-side config for changes to historical and database data on the customer.
Inactive atm.

### non-scopemap.json

Configuration for non-location items like ip_groups and operator_groups.
This is combined with the location scope names in cellmap to create the final scopemap.

DEPRECATION NOTICE: We will split the non-location items into their own configuration
file outside of scopemap.

### pois.json

The list of the customers Points of Interest (POIs).

### poi2cell.json

Association between a POI and one or more base locations (e.g. cgis in telcos)
Needed to be able to calculate aggrgeations for the POI.

### rating_apps.json

List of apps to be used in Crisis Center workspace.

Apps must be listed with their app_analytics_id.

### rating devices

List of devices to be used in Crisis Center workspace.

### scopes.json

The location scopes of the customer.

### snifraud_domains.json

Lists of domains to be used in nlive to produce SNI fraud. The suffix list does as suffix match, while the exact list does an exact domain match.

#### Meta

The meta field keeps basic information about the customer maps setup:

    "meta": {
        "nation": "Singapore", -----> the country of the customer
        "default_coordinates": [6.175, 106.8283], -------> the center of the country
        "root_is_point_scope": true, --------> whether we have geojson for the top level scope (false) or not (true)
        "poi": true, -----------> whether we have POIs defined for the customer (e.g. pois.json file is not empty)
        "cluster_poi": false ---------> whether the UI should show POIs together (consult UI mapview team for value)
    },


#### Scopes

The scopes field is a list of all active location scopes in the customer.

- scopemap_keys: how the scope is referenced in scopemap.json file
- cli_key: how the scope is referenced in nlive rpc queries
- scope_name: the primary name of the scope
- cellmap_key: how the scope is listed in cellmap.json
- analytics_key: how the scope is listed in analytics (db, etc)
- display_name: how the scope is shown in the UI

The desired goal is to kill most of the above and just use:

1) the scope_name (e.g. region) whenever we need a fixed user-friendly name in files,

2) a scope_x abstract name everywhere (from scope_1 to scope_12) everywhere else (db, rpc queries, etc)

3) a display_name to be the user-friendly UI label version (e.g. Region).

In practice they are used because of legacy differences between how different systems
refer to a location scope, and some hardcoded scopes in analytics side. Specifically,
there are several hardcoded scopes in the nlive side which need special
consideration. Those are:

11 scopes hardcoded in nlive:
- hardcoded_nlive_abstracts: scope_1, scope_2, scope_3, scope_4
- hardcoded_nlive_named: city, cluster, region, province, business_area, bussiness_sales_area, business_cluster

analytics_key should always use the abstract form (scope_x where x in 5..12)

If a new scope we want to add (e.g. "foo") is:

in hardcoded_nlive_named

------------> scopemap_key should include the hardcoded nlive name (eg. region)

in hardcoded_analytics_abstracts only:

------------> scopemap_key should include scope_x (x in 1..4)

In all cases the scope_name will be the same as the original scope (an actual name, not some scope_x form)


#### Hierarchies

The hierarchies field has a number of named hierarchies (e.g. api, mapview).

Those are used to show a subset of the location scopes in different workspaces.

The cluster attribute is used by the UI (consule mapview team for value per customer)

#### Note: fixed customers

For fixed customers, the last entry in scopes list should be the "base location"
they want to use (e.g. lto in singtel-fixed) [which might not be the actual bottom
location, e.g. subscriber home].

In fixed customers, data for the base location (what cellmap refers to as "location")
is aggregated twice, once as the last listed scope, and another as cgi.

This is done so that if the want to move the base location lower (more fine-grained)
later on, we don't have it as cgi only.


#!/usr/bin/env python
# NIO_COMPONENT analytics.maps
import json
import sys
import os


CUSTOMERS = {f for f in os.listdir("./data/customers") if not f.startswith(".")}

for c in CUSTOMERS:
    if c not in ["telkomsel"]:
        continue
    
    print "CUSTOMER: %s" % c
    dlg = {}
    dlg_file = "./data/customers/%s/config/dlg_config.json" % c

    try:
        with open(dlg_file, "r") as fp:
            dlg = json.load(fp)
    except Exception as e:
        print "File %s doesn't exist: %s" % (dlg_file, e)
        continue

    million = 1000000

    dlg["buckets"] = []
    for app in dlg["apps"]:
        dlg["buckets"].append({
            "attr_value": app,
            "attr_type": "app",
            "attr_metric": "kpi_data_volume_total_bytes",
            "buckets": [
                {
                    "from": 0,
                    "to": million,
                    "label": "Low"
                },
                {
                    "from": million,
                    "to": 10 * million,
                    "label": "Med"
                },
                {
                    "from": 10 * million,
                    "to": 20 * million,
                    "label": "test"
                },
                {
                    "from": 20 * million,
                    "to": 100 * million,
                    "label": "High"
                },
                {
                    "from": 100 * million,
                    "to": 1000 * million,
                    "label": "Very High"
                }
            ]
        })

        dlg["buckets"].append({
            "attr_value": app,
            "attr_type": "app",
            "attr_metric": "days_count",
            "buckets": [
                {
                    "from": 1,
                    "to": 5,
                    "label": "Low"
                },
                {
                    "from": 5,
                    "to": 10,
                    "label": "Med"
                },
                {
                    "from": 10,
                    "to": 15,
                    "label": "test"
                },
                {
                    "from": 15,
                    "to": 20,
                    "label": "High"
                },
                {
                    "from": 20,
                    "to": 32,
                    "label": "Very High"
                }
            ]
        })

    with open("./data/customers/%s/config/dlg_config.maker.json" % c, "w") as fp:
        json.dump(dlg, fp, indent=4)

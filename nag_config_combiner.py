#!/usr/bin/env python


import os
import sys
import json
from argparse import ArgumentParser


class Cell(object):
    def __init__(self, background="white", label="", header=False, top=False):
        self.background = background
        self.label = label or "&nbsp;"
        self.header = header
        self.top = top

    def to_html(self):
        color = "white"
        background = "gray"

        if self.background == "red":
            background = "#700A0B"
        elif self.background == "green":
            background = "#18a018"
        elif self.background == "green-dark":
            background = "#0b3f0b"

        label = self.label
        if self.top:
            label = "<u>%s</u>" % self.label

        if self.header:
            return """
                <th style="background: %s; color: %s;">%s</th>
            """ % (background, color, label)
        else:
            return """
                <td style="background: %s; color: %s;">%s</td>
            """ % (background, color, label)


class ConfigAssembler(object):

    @staticmethod
    def assemble_config():

        customers = os.listdir("data/customers")
        customers = [c for c in customers if os.path.isdir("data/customers/%s" % c) and not c.startswith(".")]

        config = {
            "configuration": [
                # {
                #     "customer": "NAME",
                #     "aggregations": [
                #         {
                #             "dimensions_combo": "combo",
                #             "enabled": true/false
                #             "filters": ...
                #         }
                #     ],
                #     "eventlog_local": [
                #         {
                #         "dimensions_combo": "combo",
                #         ...
                #         }
                #     ]
                # }
            ]
        }

        for c in customers:
            nlive_aggregations_path = "data/customers/%s/config/nlive_aggregations.json" % c
            customer_aggregations = ConfigAssembler._get_customer_aggregations(nlive_aggregations_path)

            eventlog_local_path = "data/customers/%s/config/eventlog.json.maps.local" % c
            customer_eventlog_local = ConfigAssembler._get_customer_eventlog_local(eventlog_local_path)

            cur_customer_config = {
                "customer": c,
                "aggregations": customer_aggregations,
                "eventlog_local": customer_eventlog_local,
            }

            config["configuration"].append(cur_customer_config)

        return config

    @staticmethod
    def sort(config):
        config["configuration"] = sorted(config["configuration"], key=lambda c: c["customer"])

        for c in config["configuration"]:
            c["aggregations"] = sorted(c["aggregations"], key=lambda a: a["dimensions_combo"])
            c["eventlog_local"] = sorted(c["eventlog_local"], key=lambda a: a["dimensions_combo"])

        return config

    @staticmethod
    def _get_customer_aggregations(nlive_aggregations_path):
        customer_aggregations = []

        nlive_aggregations = {}
        if os.path.exists(nlive_aggregations_path):
            with open(nlive_aggregations_path, "r") as fp:
                nlive_aggregations = json.load(fp)

        for dimensions_combo, na_conf in nlive_aggregations.items():

            aggregation_config = {
                "dimensions_combo": dimensions_combo,
                # if enabled is not set to false, it defaults to true
                "enabled": na_conf.get("enabled", True),
            }

            if "filters" in na_conf:
                aggregation_config["filters"] = na_conf.get("filters")

            customer_aggregations.append(aggregation_config)

        return customer_aggregations

    @staticmethod
    def _get_customer_eventlog_local(eventlog_local_path):
        customer_eventlog_local = []

        eventlog_local = []
        if os.path.exists(eventlog_local_path):
            with open(eventlog_local_path, "r") as fp:
                for line in fp.readlines():
                    line = line.strip()
                    if not line:
                        continue
                    if line.endswith(","):
                        line = line[:-1]
                    cur = json.loads(line)
                    eventlog_local.append(cur)

        for el in eventlog_local:
            dimensions_combo = el.get("module").replace("live_no_aggr.", "").replace("live.", "")
            el_config = {
                "dimensions_combo": dimensions_combo,
            }
            el_config.update(el)
            customer_eventlog_local.append(el_config)

        return customer_eventlog_local

    @staticmethod
    def read_all_available_combos():
        all_available_combos = []

        with open("../nio-ncore/conf/schema.json", "r") as fp:
            schemas = json.load(fp)

        for schema_id, schema_conf in schemas.items():
            schema_name = schema_conf.get("name")
            if not schema_name.startswith("live"):
                continue
            if schema_name.startswith("live_no_aggr"):
                continue
            if "obsolete" in schema_name:
                # this convers .obsolete, .obsolete2 etc
                continue

            schema_dimensions_combo = schema_name.replace("live.", "")
            all_available_combos.append(schema_dimensions_combo)

        return sorted(all_available_combos)

    @staticmethod
    def get_combo_customer_map(config):
        combo_customer_map = {}

        all_available_combos = ConfigAssembler.read_all_available_combos()

        for combo in all_available_combos:
            combo_customer_map[combo] = {}
            for ca in config["configuration"]:
                customer = ca["customer"]
                combo_customer_map[combo][customer] = {
                    "customer": customer,
                    "aggregation": None,
                }

                for ag in ca["aggregations"]:
                    if ag["dimensions_combo"] == combo:
                        combo_customer_map[combo][customer]["aggregation"] = ag
                        break

        return combo_customer_map


def main():
    parser = ArgumentParser()
    parser.add_argument("--export", action="store_true", default=False, help="Export customer config as a html table")
    parser.add_argument("--combine", action="store_true", default=False, help="Combine customer configuration into a single JSON file")

    args = parser.parse_args()

    config_assembler = ConfigAssembler()
    config = config_assembler.assemble_config()
    config = config_assembler.sort(config)

    all_available_combos = config_assembler.read_all_available_combos()

    combo_customer_map = config_assembler.get_combo_customer_map(config)

    if args.combine:
        with open("nag_customer_config.json", "w") as fp:
            json.dump(config, fp, indent=4)

        return 0

    if args.export:
        customers = [ca["customer"] for ca in config["configuration"]]

        table = []

        # first row
        row = []
        for c in customers:
            row.append(Cell(label=c, header=True))
        table.append(row)

        # TODO publish to:
        # https://confluence.niometrics.com/pages/viewpage.action?pageId=90857012
        # TODO add rules for:
        # 1. if location dimension not in scopemap.json (region, city, cluster, business_*, province, scope_N) -> disabled
        # 2. if isp, institution, institution type not in scopemap.json -> disabled
        # 3. if not fixed customer -> bras, wag, node_type disabled

        # misc rows
        for combo in all_available_combos:
            row = []
            for customer in customers:
                ca = combo_customer_map[combo][customer]["aggregation"]
                if ca is None:
                    row.append(Cell(label=combo, background="green-dark"))
                elif ca.get("enabled") is False:
                    row.append(Cell(label=combo, background="red"))
                elif ca.get("enabled"):

                    top_filtered = False
                    dimensions = ca.get("filters", {}).get("dimensions", [])
                    for d in dimensions:
                        # string pointing to analytics_id_groups, not list of values
                        if isinstance(d.get("values"), basestring):
                            top_filtered = True

                    row.append(Cell(label=combo, background="green", top=top_filtered))
                else:
                    raise Exception("Invalid state")
            table.append(row)

        res = ""
        res += """
            <style>
                table {
                    font-family: Arial, sans-serif;
                    font-size: 15px;

                }

                th, td {
                    min-width: 400px;
                    text-overflow: ellipsis;
                    width: 400px;
                    max-width: 400px;
                    overflow: hidden;
                    padding: 5px;
                }
            </style>
        """
        res += """<table border="1">"""
        res += "\n"
        for row in table:
            res += "\t<tr>"
            res += "\n"
            for cell in row:
                res += "\t\t" + cell.to_html()
            res += "\n"
            res += "\t</tr>"
            res += "\n"
        res += "</table>"
        res += "\n"

        with open("nag_customer_config.html", "w") as fp:
            fp.write(res)


if __name__ == "__main__":
    sys.exit(main())

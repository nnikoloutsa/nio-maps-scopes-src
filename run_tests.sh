#!/bin/bash
# NIO_COMPONENT analytics.maps

# Runs nio-maps-scopes tests (in the test folder) with a test-specific DB
# using the local nio-maps-scopes source code (over RPM installed code)
# To use, from your Mac:
# rbuild -Ss nio-maps-scopes
# rbuild -Ss nio-python-libs
# On your rbuild host:
# cd ~/rbuild/nio-maps-scopes && ./run_tests.sh

set -e

# check if local nio-python-libs have been uploaded too
if [ ! -d ../nio-python-libs ]; then
	echo "Sibling nio-python-libs directory not found!"
	echo "You need to rbuild -sS nio-python-libs alongside nio-maps-scopes!"
	exit 1
fi

# Prepare python path to run tests with the local API / PYTHON-LIBS
# source code -- and not the one already installed on the system as RPM
export RBUILD_API_PATH="./src"
export RBUILD_NIO_PYTHON_LIBS_PATH="../nio-python-libs/src"
export PYTHONPATH=$(
  PYTHONPATH='' \
  python -c 'import sys; \
    sys.path.extend(sys.argv[1:]); old=list(sys.path); \
    import site; site.main(); \
    [ old.append(p) for p in sys.path if p not in old ]; \
    sys.path=old; \
    print ":".join(sys.path)' \
  $RBUILD_NIO_PYTHON_LIBS_PATH $RBUILD_API_PATH)

# run tests
echo "Running tests..."
python -S -B setup.py test "${@}"

echo "Done!"

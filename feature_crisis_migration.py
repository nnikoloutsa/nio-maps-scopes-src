#!/usr/bin/env python
# NIO_COMPONENT analytics.maps

import sys
import os
import json

BASE_PATH = "./data/customers"


def main():

    customers = {f for f in os.listdir(BASE_PATH) if not f.startswith(".")}
    for c in customers:
        rating_app = "%s/%s/config/rating_apps.json" % (BASE_PATH, c)
        rating_device = "%s/%s/config/rating_devices.json" % (BASE_PATH, c)
        eventlog_maps = "%s/%s/config/eventlog.json.maps.local" % (BASE_PATH, c)
        feature_crisis = "%s/%s/config/feature.crisis.json" % (BASE_PATH, c)

        print
        print c

        if os.path.exists(feature_crisis):
            os.remove(feature_crisis)

        cnt = 0
        if os.path.exists(rating_app):
            cnt += 1

        if os.path.exists(rating_device):
            cnt += 1

        if cnt not in [0, 2]:
            raise Exception("not both files exists")

        if cnt == 0:
            print c, "does not have rating files"
            continue

        with open(rating_app, "r") as fp:
            rating_app_json = json.load(fp)

        with open(rating_device, "r") as fp:
            rating_device_json = json.load(fp)

        if not os.path.exists(eventlog_maps):
            print c, "does not have eventlog"
            continue

        os.remove(rating_app)
        os.remove(rating_device)

        has_rating = False
        with open(eventlog_maps, "r") as fp:
            eventlog = fp.read()
            for row in eventlog.split("\n"):
                row = row.strip()
                if row.endswith(","):
                    row = row[:-1]

                if row:
                    rowj = json.loads(row)
                    if "rating" in rowj["logfile"] and "scope_3" in rowj["module"]:
                        print c, rowj["logfile"], rowj["module"]
                        has_rating = True

        if not has_rating:
            print c, "does not have rating"
            continue

        with open(feature_crisis, "w") as fp:
            data = {
                "scope": "district",
                "apps": rating_app_json,
                "devices": rating_device_json,
            }
            json.dump(data, fp, indent=4)

        print c, len(rating_app_json), len(rating_device_json)

if __name__ == "__main__":
    sys.exit(main())

# NIO_COMPONENT analytics.maps
%define debug_package %{nil}

Summary: Niometrics maps backend (base)
Name: nio-maps-scopes-src
Version: %{rpm_version}
Release: %{rpm_release}
License: Proprietary
Group: Development Tools/Tools
BuildRoot: %{_tmppath}/%{name}-%{version}-root-%(%{__id_u} -n)
Packager: Nikolaos Ventouras <ventouras@niometrics.com>
Source: nio-maps-scopes-src-%{rpm_version}.tar.gz
Source1: nio-maps-scopes-pocket-%{rpm_version}.tar.gz

# Build dependencies (for mapshaper)
#BuildRequires: nodejs == 4.6.0
###BuildRequires: nodejs >= 12.16.3
###
###BuildRequires: systemd
###BuildRequires: parallel
###BuildRequires: python-setuptools
###BuildRequires: python2-devel
###BuildRequires: python-psycopg2
###BuildRequires: python-simplejson
###BuildRequires: postgresql12-devel
###BuildRequires: postgresql12-server
###BuildRequires: python-testing.postgresql
###Requires: postgresql-libs
###Requires: python >= 2.7
###Requires: python-psycopg2
###Requires: python-requests
###Requires: python-simplejson
###Requires: python-zmq
###Requires(pre): nio-conf >= 1.5.656-0.57
###Requires: nio-conf >= 1.5.656-0.57
###Requires: nio-base >= 1.5.657-0.12
###Requires: nio-storage >= 1.5.657-0.1
###Requires: postgresql12-libs
###Requires: postgresql12-server
###Requires: postgresql12-contrib
###Requires: jq
###Requires: time
###Requires: nio-maps-scopes-customer

%description
Niometrics maps backend

%define _prefix /opt/nio
%define _confdir /etc/opt/nio
%define _mapsdir %{_confdir}/maps
%define _mapsrawdir %{_confdir}/maps/raw
%define _mapsconfigdir %{_confdir}/maps/config
%define _python2_sitelib /opt/nio/lib/python2.7/site-packages

# PER CUSTOMER SUB-PACKAGES ##

###%package pocket
###Summary: Niometrics maps backend for pocket (pocket)
###Group: Development Tools/Tools
###Provides: nio-maps-scopes-customer
###
###%description pocket
###Niometrics maps backend for pocket
###
###%files pocket
###%attr(-,nio,nio) %{_mapsrawdir}/pocket
###%exclude %{_python2_sitelib}

# END SUB-PACKAGES ##

%prep

%setup -q
%setup -q -T -D -b 1

%build
rm -rf %{buildroot}

#for customer_name in data/customers/*; do
#	python locations_updater.py --customer=$(basename "$customer_name")
#done

# run building in parallel
declare -a customer_cmds
idx=0
###for customer_path in data/customers/*; do
echo "PWD"
pwd
echo "ls"
ls
echo "ls .."
ls ..
for customer_path in data/customers/*; do
	customer_name=$(basename "$customer_path")
	customer_cmds[$idx]="python locations_updater.py --customer=$customer_name"
	idx=$((idx + 1))
done

#parallel ::: 'python locations_updater.py --customer=a1hr' 'python locations_updater.py --customer=ais'

#find artifacts/customers/*

parallel --workdir . ::: "${customer_cmds[@]}"

parallel_status=$?

echo "parallel_status: $parallel_status"

#echo "Exiting forcefully"
#exit 1

python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --root=%{buildroot} --prefix=%{_prefix}
find %{buildroot} -name '*.py' -delete

install -d %{buildroot}/%{_mapsrawdir}
install -d %{buildroot}/%{_mapsconfigdir}
install -d %{buildroot}/opt/nio/libexec

cp -r artifacts/customers/* %{buildroot}/%{_mapsrawdir}

install -d %{buildroot}/%{_python2_sitelib}

%pre

%post
IFS= CUSTOMER_PATH=$(find %{_mapsrawdir} -mindepth 1 -maxdepth 1 -type d -not -name .)

echo "CUSTOMER_PATH: $CUSTOMER_PATH"

echo "Proceeding with %post argument: $1"
# only run for nio-maps-scopes (base) package, not subpackages
# $1 is >= 1 in first install (1) or update (2, etc.)
if [ "$1" -ge "1" ]; then

	# check how many directories are inside _mapsrawdir (excluding config)
	if [ $(echo "${CUSTOMER_PATH}" | wc -l) -ge "2" ]; then
	  echo "ERROR: More than one customer directory under %{_mapsrawdir} - aborting"
	  exit 1
	fi

	# if we got here, we have found 1 customer directory as expected
	echo "Found customer directory. Setting up locations..."

	CUSTOMER=$(basename $CUSTOMER_PATH)

	# delete previous maps-related symlinks
	test -d %{_mapsconfigdir} && echo "Removing previous symlinks" && rm -rf %{_mapsconfigdir}/*.json %{_mapsconfigdir}/*.txt
	# and delete badly named eventlog.json.maps.local FIXME SAPILA
	test -f %{_mapsconfigdir}/eventlog.json.maps.local && echo "Removing previous eventlog local symlink" && rm -rf %{_mapsconfigdir}/eventlog.json.maps.local

	# create symbolic link from customer maps raw files to _mapsdir/config
	for file in %{_mapsrawdir}/${CUSTOMER}/files/*; do
		filename=$(basename $file)
		raw_file="%{_mapsrawdir}/${CUSTOMER}/files/$filename"
		config_file="%{_mapsconfigdir}/$filename"

		echo "-- Linking ${config_file} -> ${raw_file}"

		ln -s "$raw_file" "$config_file"
	done

	# create symbolic links to legacy file locations (of location files)
	declare -a legacy_files=("poi2cell.json" "pois.json" "cellmap.json" "scopemap.json" "nodemap.json")

	for file in "${legacy_files[@]}"
	do
		legacy_filepath=/etc/opt/nio/ncore/$file
		rpm_filepath=%{_mapsrawdir}/${CUSTOMER}/files/$file

		# delete previous symlink or (for the first update) legacy file
		if [[ -L "$legacy_filepath" || -f "$legacy_filepath" ]]; then
			echo "Removing previous $legacy_filepath"
			rm $legacy_filepath
		fi
		# symlink to new RPM provided file
		if [ -f "$rpm_filepath" ]; then
			echo "Relinking $legacy_filepath"
			ln -s "$rpm_filepath" "$legacy_filepath"
		fi
	done

	is_mrs=$(/usr/bin/jq -r '.is_mrs' < /etc/opt/nio/nio_maps.conf.json)
	is_dw=$(/usr/bin/jq -r '.is_dw' < /etc/opt/nio/nio_maps.conf.json)

	# load locations data into postgres on MRS or DW machines
	if [[ $is_mrs == "true" || $is_dw == "true" ]]; then
		echo "MRS or DW system: starting locations update..."

		if [ -f %{_mapsconfigdir}/locations.json ]; then

			echo "Loading locations..."
			# we load the locations to the DB
			/opt/nio/bin/locations_loader --locations-file=%{_mapsconfigdir}/locations.json
		else
			echo "[WARNING] locations.json file not found in maps config dir. API locations db table will not be filled!"
		fi

		if [ -f %{_mapsconfigdir}/locations_gis.json ]; then

			echo "Loading locations_gis..."
			# we load the locations_gis to the DB
			if /opt/nio/bin/locations_gis_loader --fname=%{_mapsconfigdir}/locations_gis.json; then
				echo "locations_gis table loaded with data - exit status $?"
			else
				echo "exit status $? from locations_gis_loader (failed)"
			fi
		else
			echo "[WARNING] locations_gis.json file not found in maps config dir. API locations_gis db table will not be filled!"
		fi

	fi

	echo "Done. Phew!"

fi


%postun
/usr/bin/true

%clean
rm -rf %{buildroot}

%files
/opt/nio/bin/locations_loader
/opt/nio/bin/locations_gis_loader
/opt/nio/bin/historical_migrator
/opt/nio/lib/python2.7/site-packages/nio/maps
/opt/nio/lib/python2.7/site-packages/nio_maps_scopes-0.1-py2.7-nspkg.pth
/opt/nio/lib/python2.7/site-packages/nio_maps_scopes-0.1-py2.7.egg-info/
%exclude %{_mapsrawdir}
%dir %{_mapsconfigdir}


%changelog
* Wed Apr 04 2018 Nikolaos Ventouras <ventouras@niometrics.com>
- Mostly working!
* Tue Feb 13 2018 Nikolaos Ventouras <ventouras@niometrics.com>
- Initial packaging


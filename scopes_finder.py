#!/usr/bin/env python
# NIO_COMPONENT analytics.maps
import json
import sys
import os


CUSTOMERS = {f for f in os.listdir("./data/customers") if not f.startswith(".")}

for c in CUSTOMERS:
    scopes = {}
    scopes_file = "./data/customers/%s/config/scopes.json" % c

    if os.path.exists(scopes_file):
        with open(scopes_file, "r") as fp:
            scopes = json.load(fp)

    for s in scopes["scopes"]:
        if "analytics_key" in s:
            if "district" in s["scope_name"]:
                print "CUSTOMER: %s" % c
                print "%s -> %s, scopemap_keys:" % (s["scope_name"], s["analytics_key"]), s["scopemap_keys"], s["aggregations"] if "aggregations" in s else None

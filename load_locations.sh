#!/bin/bash
# NIO_COMPONENT analytics.maps

set -u
set -e

CUSTOMER=$1
HOST=$2

echo "Deploying locations to mrs: ${HOST} for customer: ${CUSTOMER}"

locations_path="./artifacts/customers/${CUSTOMER}/files/locations.json"

if [ ! -f "${locations_path}" ]; then
	echo "Locations file cannot be found!"
	exit 1
fi

scp -C "${locations_path}" "${HOST}":/var/tmp/maps_locations.json
scp -C "src/nio/maps/scripts/locations_loader.py" "${HOST}":/var/tmp/locations_loader.py

ssh "${HOST}" <<'ENDSSH'
	cd /var/tmp
	sudo python /var/tmp/locations_loader.py --locations-file /var/tmp/maps_locations.json
	sudo mv /var/tmp/maps_locations.json /var/tmp/maps_locations.json.loaded
	sudo rm /var/tmp/locations_loader.py
ENDSSH

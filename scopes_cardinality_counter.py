import os
import json

def get_scope_count(cellmap, cellmap_keys):
    scope_set_map = {}
    for e in cellmap:
        for key in cellmap_keys:
            if key not in scope_set_map:
                scope_set_map[key] = set()
            value = e.get(key)
            if value:
                scope_set_map[key].add(value)

    scope_count_map = {}
    for key in scope_set_map:
        scope_count_map[key] = len(scope_set_map[key])

    return scope_count_map

def count_hierarchy_items(customer):
    config_dir = "data/customers/%s/config" % customer
    scopes_file = config_dir + "/scopes.json"
    cellmap_file = config_dir + "/cellmap.json"

    with open(scopes_file, "r") as fp:
        scopes = json.load(fp)

    with open(cellmap_file, "r") as fp:
        cellmap = json.load(fp)

    hierarchy = scopes.get("hierarchies").get("mapview")

    scope_name_to_cellmap_name = {}
    for s in scopes.get("scopes"):
        scope_name_to_cellmap_name[s.get("scope_name")] = s.get("cellmap_key")

    cellmap_keys = [scope_name_to_cellmap_name[he["scope_name"]] for he in hierarchy]
    scopes_count = get_scope_count(cellmap, cellmap_keys)

    # print customer
    # for he in hierarchy:
    #     scope_name = he["scope_name"]
    #     cellmap_key = scope_name_to_cellmap_name[scope_name]
    #     print scope_name, scopes_count[cellmap_key]

    # print "-"*100

    print customer, "N2:"
    hierarchy_n2 = hierarchy[:2]
    for he in hierarchy_n2:
        scope_name = he["scope_name"]
        cellmap_key = scope_name_to_cellmap_name[scope_name]
        print scope_name, scopes_count[cellmap_key]

    print "-"*100


for customer in os.listdir("data/customers"):
    if customer == ".DS_Store":
        continue
    count_hierarchy_items(customer)



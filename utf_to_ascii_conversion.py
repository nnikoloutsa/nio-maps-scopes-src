# -*- coding: utf-8 -*-

import io
import sys
import codecs

sys.stdout = codecs.getwriter("UTF-8")(sys.stdout)

original = u"ČĆčĐđéŠšŽž"
substitu = u"CCcDdeSsZz"

with io.open("pois.json", encoding="utf-8") as fp:
    for line in fp.readlines():

        line = line.strip()

        for i, org in enumerate(original):
            sub = substitu[i]

            line = line.replace(org, sub)

        print line


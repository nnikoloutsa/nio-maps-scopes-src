#!/usr/bin/env python
# NIO_COMPONENT analytics.maps

import sys
import os
from argparse import ArgumentParser

sys.path.append("./src")
# should be behind the sys.path.append
from nio.maps.tools import locations_updater


def main():

    parser = ArgumentParser()
    parser.add_argument("--customer", type=str, required=True, help="Customer id")
    parser.add_argument("--save-optimization", action="store_true", default=False, help="Save optimization data")

    args = parser.parse_args()

    CUSTOMERS = {f for f in os.listdir("./data/customers") if not f.startswith(".")}

    if args.customer not in CUSTOMERS:
        print "Unknown customer specified: %s" % args.customer
        return 1

    reader = locations_updater.locations_updater(args.customer, save_optimization=args.save_optimization)
    if reader:
        reader.print_errors()


if __name__ == "__main__":
    sys.exit(main())

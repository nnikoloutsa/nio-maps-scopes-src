#!/bin/bash
# NIO_COMPONENT analytics.maps

set -u
set -e

CUSTOMER=$1
HOST=$2

echo "Deploying locations to mrs: ${HOST} for customer: ${CUSTOMER}"

locations_gis_path="./artifacts/customers/${CUSTOMER}/files/locations_gis.json"

if [ ! -f "${locations_gis_path}" ]; then
	echo "Locations GIS file cannot be found!"
	exit 1
fi

scp -C "${locations_gis_path}" "${HOST}":/var/tmp/maps_gis_locations.json
scp -C "src/nio/maps/scripts/locations_gis_loader.py" "${HOST}":/var/tmp/locations_gis_loader.py

ssh "${HOST}" <<'ENDSSH'
	cd /var/tmp
	sudo python /var/tmp/locations_gis_loader.py --fname /var/tmp/maps_gis_locations.json
	sudo mv /var/tmp/maps_gis_locations.json /var/tmp/maps_gis_locations.json.loaded
	sudo rm /var/tmp/locations_gis_loader.py
ENDSSH

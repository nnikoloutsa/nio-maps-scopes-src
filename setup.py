#
# NIO_COMPONENT analytics.maps
#
from setuptools import setup, find_packages

setup(
    name='nio-maps-scopes',
    version='0.1',
    author='Psimaris Dimitris',
    author_email='psimaris@niometrics.com',
    license='Proprietary',
    namespace_packages=['nio'],
    packages=find_packages('src'),
    package_dir={'': 'src'},
    zip_safe=False,
    test_suite='tests',
    install_requires=[],
    entry_points={
        'console_scripts': [
            'locations_loader=nio.maps.scripts.locations_loader:main',
            'locations_gis_loader=nio.maps.scripts.locations_gis_loader:main',
            'historical_migrator=nio.maps.scripts.historical_migrator:main'
        ],
    }
)

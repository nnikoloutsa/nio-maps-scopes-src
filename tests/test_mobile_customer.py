#
# NIO_COMPONENT analytics.maps
#
import os
import shutil
import unittest
import filecmp
from nio.maps.tools import locations_updater


def test_customer(customer):
    input_path = "tests/data"
    output_path = "tests/data/output"
    expected_path = "%s/customers/%s/expected" % (input_path, customer)
    expected_path_output = "%s/customers/%s/files" % (output_path, customer)

    locations_updater.locations_updater(customer, input_path=input_path, output_path=output_path)

    failure = False
    for file in os.listdir(expected_path):
        if file.startswith("."):
            continue

        same = filecmp.cmp("%s/%s" % (expected_path, file), "%s/%s" % (expected_path_output, file))
        if not same:
            print "Failure file: %s" % file
            failure = True

    if os.path.exists(output_path):
        shutil.rmtree(output_path)

    return failure


class TestMobileCustomer(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestMobileCustomer, cls).setUpClass()

    def test_mobile_customer(self):
        failure = test_customer("mobile_customer")
        self.assertEqual(failure, False)


def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestMobileCustomer))
    return test_suite


test_suite = suite()

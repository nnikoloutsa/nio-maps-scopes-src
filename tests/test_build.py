#
# NIO_COMPONENT analytics.maps
#
import os
import shutil
import unittest
from nio.maps.tools import locations_updater


class TestBuild(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestBuild, cls).setUpClass()

    def test_build(self):
        CUSTOMERS = {f for f in os.listdir("./data/customers") if not f.startswith(".")}
        failures = []
        reader_per_customer = {}
        for c in CUSTOMERS:
            try:
                reader = locations_updater.locations_updater(c)
                reader_per_customer[c] = reader
                files_path = "artifacts/customers/%s/files" % c
                if os.path.exists(files_path):
                    shutil.rmtree(files_path)
            except Exception, e:
                failures.append({
                    "customer": c,
                    "error": str(e)
                })

        for customer, reader in reader_per_customer.items():
            print "Errors for customer: %s" % customer
            if reader:
                reader.print_errors()

        self.assertEqual(len(failures), 0)


def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestBuild))
    return test_suite


test_suite = suite()

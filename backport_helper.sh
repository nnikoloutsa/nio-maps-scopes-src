#!/bin/bash

set -e

function _echo_error() {
  echo -n "$(tput setaf 1)"
  echo -n "$1"
  echo "$(tput sgr0)"
}

function _echo_ok() {
  echo -n "$(tput setaf 2)"
  echo -n "$1"
  echo "$(tput sgr0)"
}

function _echo_command() {
  echo -n "$(tput setaf 3)"
  echo -n "${@}"
  echo "$(tput sgr0)"
}

function _print_unexpected_error() {
  _echo_error "There was an unexpected error!"
  exit
}


function _ensure_confirmation() {
  read -r -p "This script is intended for maps-scopes maintainers. Continue? (y/n): " response
  case "$response" in
    y|Y)
      return 0
      ;;
    n|N)
    _echo_ok "Bye. Don't let the door hit you on your way out!"
    exit
      ;;
    *)
    _echo_ok "Please enter 'y' (yes) or 'n' (no)"
      return 1
      ;;
  esac
}

# because we've set "exit immediately on error" on top
# we need to trap uncaught errors to at least get a message
# before exiting
trap _print_unexpected_error ERR

# ==========================================================

# Reset in case getopts has been used previously in the shell
OPTIND=1

#### DEFAULT ARGUMENT VALUES ################################
CUSTOMER=""

BRANCH=""

FILES=""

BACKPORT_GEOJSON="false"

######################### PARSE CLI ARGUMENTS ################
function show_help() {
  echo "Usage: create_postgres.db [ -p <PATH_TO_POSTGRES_DATA_DIR> -v <VERSION> -P <PORT> -d <DATABASE> | -h]"
  echo "E.g.  create_postgres.db -p /home/me/pg_datadir -v 12 -d analytics_test -P 6666"
}

function _print_usage_and_exit() {
  _echo_error "Usage: backport_helper.sh -c <customer> -b <stable_branch> -f [<default-list> | <comma separated files>] [-g] [-h]"
  _echo_error "Flags:"
  _echo_error $(printf "\t")"-c: which customer to backport data for"
  _echo_error $(printf "\t")"-b: branch to backport to (from master)"
  _echo_error $(printf "\t")"-f: which config files to backport (default-list or explicit list)"
  _echo_error $(printf "\t")"-g: also backport geojson"
  _echo_error $(printf "\t")"-h: print this and exit"

  exit
}

while getopts "h?c:b:f:g?" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    c)  CUSTOMER="$OPTARG"
        ;;
    b)  BRANCH="$OPTARG"
        ;;
    f)  FILES="$OPTARG"
        ;;
    g)  BACKPORT_GEOJSON="true"
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

###################################################

if [ "$CUSTOMER" == "" ]; then
  _print_usage_and_exit
fi

if [ "$BRANCH" == "" ]; then
  _print_usage_and_exit
fi

if [ ! -d "./data/customers/${CUSTOMER:-EMPTY}" ]; then
  _echo_error "Customer data directory doesn't exist! Exiting!"
  exit
fi

branch_exists=$(git branch -a | grep -E "${BRANCH}$") || true

if [ "${branch_exists}" == "" ]; then
  _echo_error "Specified branch doesn't exist. Exiting!"
  exit
fi

until _ensure_confirmation; do : ; done

# where to temporarily store the files to backport
BACKPORT_DIR=./tmp_backport

# directory with data to backport
CUSTOMER_DATA_DIR="./data/customers/${CUSTOMER}/"

# ensure we're on the master branch
git checkout master

# delete any stale tmp backport dir
rm -rf "${BACKPORT_DIR:-EMPTY}"

# re-create the backport dir and subdirs we need
mkdir -p "${BACKPORT_DIR}/config"
mkdir -p "${BACKPORT_DIR}/geojson"

# copy files from customer config dir to the (untracked/transient) tmp backport directory
if [[ $FILES == "default-list" ]]; then
  declare -a files_to_copy=("non-scopemap.json" "cellmap.json" "poi2cell.json" "pois.json")
else
  if [[ "${FILES}" == "" ]]; then
    _echo_error "No default-list or explicit comma separated file list provided!"
    exit 1
  fi

  # user passed a comma separated list of files -- use those
  declare -a files_to_copy=($(echo "${FILES}" | tr "," "\n"))
fi

for file in "${files_to_copy[@]}"
do
  echo "Copying file: ${file}"
  test -e "${CUSTOMER_DATA_DIR:-EMPTY}/config/${file}" && \cp -r "${CUSTOMER_DATA_DIR:-EMPTY}/config/${file}" "${BACKPORT_DIR:-EMPTY}/config/"
done

if [[ "${BACKPORT_GEOJSON}" == "true" ]]; then
  test -d "${CUSTOMER_DATA_DIR:-EMPTY}/geojson" && \cp -r "${CUSTOMER_DATA_DIR:-EMPTY}/geojson/"* "${BACKPORT_DIR:-EMPTY}/geojson/"
fi

# find a name for the local branch with the backport
TMP_BRANCH_NAME=tmp_${BRANCH}_$(date +%s)

# checkout the stable remote branch as the local tmp backport branch
git checkout -b "${TMP_BRANCH_NAME}" "remotes/origin/${BRANCH}"

# copy files from the master's tmp backport dir to the customer
\cp -r "${BACKPORT_DIR:-EMPTY}"/config/* "${CUSTOMER_DATA_DIR}/config/"

if [[ "${BACKPORT_GEOJSON}" == "true" ]]; then
  \cp -r "${BACKPORT_DIR:-EMPTY}"/geojson/* "${CUSTOMER_DATA_DIR}/geojson/"
fi

# compare with previous customer config, and stage modified files only for commit
git add "${CUSTOMER_DATA_DIR}/config"

if [[ "${BACKPORT_GEOJSON}" == "true" ]]; then
  git add "${CUSTOMER_DATA_DIR}/geojson"
fi

# delete any untracked/new files in the customer config dir added in master
git clean -f "${CUSTOMER_DATA_DIR:-EMPTY}/config"
if [[ "${BACKPORT_GEOJSON}" == "true" ]]; then
  git clean -f "${CUSTOMER_DATA_DIR:-EMPTY}/geojson"
fi

# we leave the rest of the steps to human to run manually
# and supervise / revert as needed
_echo_ok "# Ready! Changed files have been added. Now run:"
_echo_command $'\t' git status
_echo_ok "# to see the changes,"
_echo_command $'\t' git commit -m "\"backported changes from staging (master) for ${CUSTOMER}\""
_echo_ok "# to commit to the temporary branch"
_echo_ok "# Then run:"
_echo_command $'\t' git push origin HEAD:"${BRANCH:-EMPTY}"
_echo_ok "# to push your changes to the origin stable branch"
_echo_ok "# and finally,"
_echo_command $'\t' git checkout master
_echo_command $'\t' git branch --delete "${TMP_BRANCH_NAME:-EMPTY}"
_echo_command $'\t' rm -rf "${BACKPORT_DIR:-EMPTY}"
_echo_ok "# to cleanup the temporary directory and branch for the backport"

echo

_echo_ok "Thank you for using backport_helper, have a nice day!"
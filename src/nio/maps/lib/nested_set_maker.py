# NIO_COMPONENT analytics.maps
from location import LocationKey


class NestedSetMaker(object):
    def __init__(self, locations):
        self.locations = locations
        self.count = 1
        self.parent_to_children_map = self._get_parent_to_children_map()
        self.updates = {}

    @staticmethod
    def _check_for_cycles(g):
        path = set()
        visited = set()

        def visit(vertex):
            if vertex in visited:
                return False
            visited.add(vertex)
            path.add(vertex)
            for neighbour in g.get(vertex, ()):
                if neighbour in path or visit(neighbour):
                    print "Cyclic:", vertex, neighbour
                    return "Cyclic path", vertex, neighbour
            path.remove(vertex)
            return False

        for i, v in enumerate(g):
            res = visit(v)
            if i % 10000 == 0:
                print "\tCycle detector - processed %s" % i
            if res is not False:
                print res
                raise Exception("Cyclic path found!!")

    def _get_parent_to_children_map(self):

        parent_to_children_map = {}

        for i, (child_key, row) in enumerate(self.locations.items()):
            # For larger sets, report every 100,000 items loaded
            if i % 100000 == 0 and i > 0:
                print "\tLoading adjacency list - processed: %s" % i

            parent_key = LocationKey(scope_name=row.parent_scope_name, id_=row.parent_id)

            if child_key == parent_key:
                raise Exception("Illegal parent-child location cycle detected")

            if parent_key not in parent_to_children_map:
                parent_to_children_map[parent_key] = []
            parent_to_children_map[parent_key].append(child_key)

        print "\tLoaded adjacency list - processed: %s" % i

        self._check_for_cycles(parent_to_children_map)
        print "\tNo cycles detected..."

        return parent_to_children_map

    def traverse(self, scope_name, id_):
        left = self.count
        self.count += 1

        key = LocationKey(scope_name=scope_name, id_=id_)
        children = self.get_children(key)

        if children:
            for child_key in children:
                self.traverse(child_key.scope_name, child_key.id)

        right = self.count
        self.count += 1
        self.updates[key] = (left, right)

    def print_tree(self, key, depth=0):
        loc = self.locations[key]
        print "%s%s (%s)" % (depth * "\t", loc.name, key.scope_name)
        for child_key in self.get_children(key):
            self.print_tree(child_key, depth + 1)

    def get_children(self, key):
        return self.parent_to_children_map.get(key, [])

    def get_nested_locations(self):

        for i, (key, (left, right)) in enumerate(self.updates.items()):
            loc = self.locations[key]
            loc.left = left
            loc.right = right
            self.locations[key] = loc

        return self.locations

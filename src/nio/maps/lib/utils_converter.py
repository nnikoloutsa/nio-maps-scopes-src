# NIO_COMPONENT analytics.maps
scope_char_replace = {
    "/": "%%%",
}


def scope_name_to_path(scope_name):
    scope_path = scope_name
    for char, value in scope_char_replace.items():
        scope_path = scope_path.replace(char, value)

    return scope_path


def scope_path_to_name(scope_path):
    scope_name = scope_path
    for char, value in scope_char_replace.items():
        scope_name = scope_name.replace(value, char)

    return scope_name

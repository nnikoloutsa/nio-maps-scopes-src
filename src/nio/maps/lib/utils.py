# NIO_COMPONENT analytics.maps
import json
import copy
import geo_helper
from optimizer import Optimizer
from static import ROOT_PARENT_SCOPE_NAME, ROOT_SCOPE_ID, ROOT_SCOPE_NAME, BASE_SCOPE_NAME
from location import LocationKey, LocationInfo
from nested_set_maker import NestedSetMaker
from helper import ProgessPercentage

# requires node package "mapshaper" available


def read_scope_hierarchy(scopes_info, hierarchy_name, include_root=False, include_leaf=False):

    hierarchy = scopes_info["hierarchies"][hierarchy_name]
    hierarchy_levels = hierarchy
    hierarchy_include_root = True
    hierarchy_include_leaf = True
    if not isinstance(hierarchy, list):
        hierarchy_levels = hierarchy["levels"]
        hierarchy_include_root = hierarchy["include_root"]
        hierarchy_include_leaf = hierarchy["include_leaf"]

    scope_hierarchy = []

    for i, hl in enumerate(hierarchy_levels):
        hierarchy_info = copy.deepcopy(next((info for info in scopes_info["scopes"] if info["scope_name"] == hl["scope_name"]), None))
        if i == 0:
            hierarchy_info["parent_scope_name"] = ROOT_SCOPE_NAME
        else:
            hierarchy_info["parent_scope_name"] = hierarchy_levels[i - 1]["scope_name"]
        hierarchy_info["cluster"] = hl.get("cluster", False)
        scope_hierarchy.append(hierarchy_info)

    if include_root and hierarchy_include_root:
        root_is_point_scope = scopes_info["meta"].get("root_is_point_scope", True)

        scope_hierarchy = [{
            "scope_name": ROOT_SCOPE_NAME,
            "scopemap_keys": [ROOT_SCOPE_NAME],
            "parent_scope_name": ROOT_PARENT_SCOPE_NAME,
            "point": root_is_point_scope,
            "cluster": False,
            "geojson.deduce_polygon": False,
            "geojson.deduce_centroid": False,
            "geojson.simplification_level": "",
            "display_name": ROOT_SCOPE_NAME.upper()
        }] + scope_hierarchy

    if include_leaf and hierarchy_include_leaf:
        parent_scope_name = scope_hierarchy[-1]["scope_name"] if len(scope_hierarchy) else ROOT_PARENT_SCOPE_NAME
        leaf_display_name = scopes_info["location_display_name"] if "location_display_name" in scopes_info else "CGI"
        scope_hierarchy += [{
            "scope_name": BASE_SCOPE_NAME,
            "analytics_key": BASE_SCOPE_NAME,
            "scopemap_keys": [BASE_SCOPE_NAME],
            "parent_scope_name": parent_scope_name,
            "point": True,
            "cluster": False,
            "geojson.deduce_polygon": False,
            "geojson.deduce_centroid": False,
            "geojson.simplification_level": "",
            "display_name": leaf_display_name
        }]

    for i, h in enumerate(scope_hierarchy):
        h["order"] = i + 1

    return scope_hierarchy


def _extract_location_map_from_cells(scopes_info, scope_hierarchy, oracle, cellmap):
    print "Creating location map from cellmap..."

    locations_map = {}

    if scope_hierarchy:
        cell_parent_scope_name = scope_hierarchy[-1]["scope_name"]
    else:
        cell_parent_scope_name = ROOT_SCOPE_NAME

    scope_to_parent_scope = {cur["scope_name"]: cur["parent_scope_name"] for cur in scope_hierarchy}
    scope_to_analytics_key = {cur["scope_name"]: cur["analytics_key"] if "analytics_key" in cur else cur["scope_name"] for cur in scope_hierarchy}

    pp = ProgessPercentage("\tProcessing base scopes (%s)" % BASE_SCOPE_NAME, len(cellmap))
    for i, c in enumerate(cellmap.values()):

        # add parent scopes
        for si in scope_hierarchy:
            scope_name = si["scope_name"]
            name = c[scope_name]
            id_ = oracle.get_location_id_from_name(scope_name, name)

            parent_scope_name = scope_to_parent_scope[scope_name]
            if parent_scope_name == ROOT_SCOPE_NAME:
                parent_name = scopes_info["meta"].get(ROOT_SCOPE_NAME, ROOT_SCOPE_NAME)
                parent_id = ROOT_SCOPE_ID
            else:
                parent_name = c[parent_scope_name]
                parent_id = oracle.get_location_id_from_name(parent_scope_name, parent_name)

            key = LocationKey(scope_name=scope_name, id_=id_)

            # TODO: we can read centroid if this is empty (None?)
            # and register the location info to print a "missing geojson later"
            geojson = oracle.get_geojson_for_location(scope_name, id_)

            locations_map[key] = LocationInfo(
                scope_name=scope_name,
                analytics_key=si.get("analytics_key", scope_name),
                parent_analytics_key=scope_to_analytics_key.get(parent_scope_name, parent_scope_name),
                id_=id_,
                name=name,
                parent_scope_name=parent_scope_name,
                parent_id=parent_id,
                coordinates="",
                geojson=geojson,
                mcc="",
                lon=None,
                lat=None,
                left=-1,
                right=-1
            )

        # add base scope itself
        v_name = None
        if cell_parent_scope_name != ROOT_SCOPE_NAME:
            v_name = c[cell_parent_scope_name]

        cell_parent_id = oracle.get_location_id_from_name(cell_parent_scope_name, v_name)

        # convert cellmap coordinates to {lat, lon} dict
        cellmap_coordinates = cellmap[c["location"]]["geoloc_simple"]
        assert len(cellmap_coordinates) == 2, "Cgi: %s does not have valid coordinates: %s!" % (c["location"], cellmap_coordinates)
        cell_point = geo_helper.Coord(lat=cellmap_coordinates[0], lon=cellmap_coordinates[1])

        cell_key = LocationKey(scope_name=BASE_SCOPE_NAME, id_=i + 1)

        locations_map[cell_key] = LocationInfo(
            scope_name=BASE_SCOPE_NAME,
            analytics_key=BASE_SCOPE_NAME,
            parent_analytics_key=scope_to_analytics_key.get(cell_parent_scope_name, cell_parent_scope_name),
            id_=i + 1,
            name=c["location"],
            parent_scope_name=cell_parent_scope_name,
            parent_id=cell_parent_id,
            coordinates=json.dumps(cell_point.getCoord()),
            geojson=json.dumps(cell_point.geojson()),
            mcc="",
            lon=cellmap_coordinates[1],
            lat=cellmap_coordinates[0],
            left=-1,
            right=-1
        )

        pp.print_progress(i)

    # add root_scope e.g. nation
    root_scope_name = scopes_info["meta"].get(ROOT_SCOPE_NAME, ROOT_SCOPE_NAME)
    coords = scopes_info["meta"].get("default_coordinates", [])

    # FIXME we could get actual GEOJSON for countries
    root_scope_geojson = json.dumps(geo_helper.Coord(coords[0], coords[1]).geojson())
    locations_map[LocationKey(scope_name=ROOT_SCOPE_NAME, id_=-1)] = LocationInfo(
        scope_name=ROOT_SCOPE_NAME,
        analytics_key=ROOT_SCOPE_NAME,
        parent_analytics_key=scope_to_analytics_key.get(ROOT_PARENT_SCOPE_NAME, ROOT_PARENT_SCOPE_NAME),
        id_=ROOT_SCOPE_ID,
        name=root_scope_name,
        parent_scope_name=ROOT_PARENT_SCOPE_NAME,
        parent_id=0,
        coordinates="",
        mcc="",
        lon=None,
        lat=None,
        geojson=root_scope_geojson,
        left=-1,
        right=-1
    )

    return locations_map


def get_hierarchies(scopes_info, hierarchy_names):
    hierarchies = {}
    for hierarchy in hierarchy_names:
        hierarchies[hierarchy] = read_scope_hierarchy(scopes_info, hierarchy, include_root=True, include_leaf=True)

    return hierarchies


def get_global_locations(countries):
    global_locations = []

    # country_information.json is basically ncore/conf/mcc-mnc.json flattened + 3 letter iso
    for i, ci in enumerate(countries):
        country_name = ci["country"]
        geojson = ci.get("geojson", None)

        if not geojson:
            # skipping locations without geojson since
            # showing them on the map is all we want them for
            continue

        # try to see if we have country level lon/lat info
        lon = ci.get("lon", None)
        lat = ci.get("lat", None)

        if lon not in [None, ""] and lat not in [None, ""]:
            country_center = geo_helper.Coord(lat=lat, lon=lon)
        else:
            # calculate from geojson points (experimental, might suck)
            locs = [geo_helper.Coord(loc[1], loc[0]) for loc in geo_helper.flatten(geojson["geometry"]["coordinates"])]
            country_center = geo_helper.calculate_midpoint(locs)

        cur_country = LocationInfo(
            scope_name="country",
            analytics_key="",
            parent_analytics_key="",
            id_=i,
            name=country_name,
            parent_scope_name="global",
            parent_id=-1,
            coordinates=json.dumps(country_center.getCoord()),
            geojson=json.dumps(geojson),
            mcc=ci["mcc"],
            lon=None,
            lat=None,
            left=-1,
            right=-1
        )

        global_locations.append(cur_country)

    return [nl._asdict() for nl in global_locations]


def get_locations_for_hierarchy(hierarchy, config_reader, save_optimization):

    scope_hierarchy = read_scope_hierarchy(config_reader.scopes_info, hierarchy)

    locations_map = _extract_location_map_from_cells(scopes_info=config_reader.scopes_info,
                                                     scope_hierarchy=scope_hierarchy,
                                                     oracle=config_reader.oracle,
                                                     cellmap=config_reader.cellmap)

    print "Transforming locations into nested set..."

    nsm = NestedSetMaker(locations_map)
    nsm.traverse(ROOT_SCOPE_NAME, ROOT_SCOPE_ID)
    locations_map = nsm.get_nested_locations()

    print "Deducing polygons for locations..."

    opt = Optimizer(scope_hierarchy=scope_hierarchy,
                    oracle=config_reader.oracle,
                    cellmap=config_reader.cellmap,
                    geojson_path=config_reader.geojson_path,
                    save_optimization=save_optimization)

    locations_map = opt.polygon_locations(locations_map)

    print "Simplifying and interpolating locations..."

    locations_map = opt.simplify_locations(locations_map)

    print "Deducing centroids for locations..."

    locations_map = opt.centroid_locations(locations_map)

    assert len(locations_map.keys()) > 0, "Empty location_map!"

    locations = []
    locations_per_scope_count = {}
    for nl in locations_map.values():
        # create a flat list of hierarcy to location entries
        # locations -> "<hierarchy_name>": [ ... {location}, ...]
        locations.append(nl._asdict())
        # and keep a count of locations per scope found, e.g.
        # locations_per_scope_count -> {"cgi": 32688, "region": 9, "site": 2484, "district": 25, "nation": 1}
        scope_name = nl.scope_name
        locations_per_scope_count[scope_name] = locations_per_scope_count.get(scope_name, 0) + 1

    return locations, locations_per_scope_count

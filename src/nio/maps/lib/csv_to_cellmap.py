# NIO_COMPONENT analytics.maps
import geo_helper

DEFAULT_LAT = 0.0
DEFAULT_LON = 0.0


def _unique_cellmap_column(data, column):
    column_set = set()
    for c in data:
        column_set.add(c[column])

    return column_set


def _print_wrong_parents(parents):
    count_invalid = 0
    count_valid = 0

    heading_printed = False
    for level, level_dict in parents.items():
        for location, loc_set in level_dict.items():
            if len(loc_set) <= 1:
                count_valid += 1
                continue

            if not heading_printed:
                print "Badly nested locations (multiple parents):"
                heading_printed = True

            print "\t%s) level: %s, location: %s -- parents: %s" % ("{:,}".format(count_invalid), level, location, list(loc_set))
            count_invalid += 1

    print "Well-nested location columns (no multiple parents): %s" % "{:,}".format(count_valid)


def _check_well_nested_columns(agg_row, parents, hierarchy):

    # check if all hierarchy entries are well-nested (no multiple parents)
    for idx, h in enumerate(hierarchy):
        value = agg_row[h]
        parent_value = None
        if idx > 0:
            parent_value = agg_row[hierarchy[idx - 1]]

        if value not in parents[h]:
            parents[h][value] = set()

        parents[h][value].add(parent_value)

        if len(parents[h][value]) > 1:
            return False

    return True


def is_inside_boundaries(lon, lat, valid_polygon):
    return lon > valid_polygon["left"] and lon < valid_polygon["right"] and lat < valid_polygon["top"] and lat > valid_polygon["bottom"]


def _aggregate_hierarchy_cellmap(cellmap, hierarchy, last_hierarchy_item, valid_polygon):
    wrong_lat_lon_lines = []
    invalid_lat_lon_lines = 0
    reverted_lat_lon_lines = 0
    cellmap_aggr = {}

    for c in cellmap:
        try:
            lat = float(c["lat"])
            lon = float(c["lon"])
            if lat == 0.0 or lon == 0.0:
                invalid_message = "goeloc not applicable, [%s, %s]" % (lat, lon)
                raise Exception(invalid_message)

            if is_inside_boundaries(lon, lat, valid_polygon):
                pass
            elif is_inside_boundaries(lat, lon, valid_polygon):
                tmp = c["lat"]
                c["lat"] = c["lon"]
                c["lon"] = tmp
                reverted_lat_lon_lines += 1
                raise Exception("Probably reverted")
            else:
                invalid_lat_lon_lines += 1
                # print "lon: %s, lat: %s" % (c["lon"], c["lat"])
                raise Exception("Not inside valid polygon")

            c["default_lat_lon"] = False
        except Exception:
            c["lat"] = DEFAULT_LAT
            c["lon"] = DEFAULT_LON
            c["default_lat_lon"] = True
            wrong_lat_lon_lines.append(c)

        key = c[last_hierarchy_item]
        agg_row = {}
        for idx, h in enumerate(hierarchy):
            agg_row[h] = c[h]

        if key not in cellmap_aggr:
            cellmap_aggr[key] = {
                "agg_row": agg_row,
                "lines": []
            }

        cellmap_aggr[key]["lines"].append(c)

    return wrong_lat_lon_lines, invalid_lat_lon_lines, reverted_lat_lon_lines, cellmap_aggr


def create_reduced_hierarchy_cellmap(cellmap, hierarchy, mapping_identifier, valid_polygon):

    base_location = hierarchy[-1]

    wrong_lat_lon_lines, invalid_lat_lon_lines, reverted_lat_lon_lines, cellmap_aggr = _aggregate_hierarchy_cellmap(cellmap, hierarchy, base_location, valid_polygon)

    aggregated_lines_introducing_multiple_parents = []
    parents = {}
    for h in hierarchy:
        parents[h] = {}

    data = []
    default_lat_lon_count = 0
    for idx, cellmap_aggr_item in cellmap_aggr.items():

        agg_row = cellmap_aggr_item["agg_row"]
        is_valid = _check_well_nested_columns(agg_row, parents, hierarchy)
        if not is_valid:
            aggregated_lines_introducing_multiple_parents.append(agg_row)
            #continue

        lines = cellmap_aggr_item["lines"]
        points = [geo_helper.Coord(lat=float(line["lat"]), lon=float(line["lon"])) for line in lines if line["default_lat_lon"] is False]

        if len(points) > 0:
            centroid = geo_helper.calculate_midpoint(points)
            coord = centroid.getCoord()
        else:
            coord = {"lat": DEFAULT_LAT, "lon": DEFAULT_LON}
            default_lat_lon_count += 1

        # if agg_row["sto"] == "TSE":
        #     print cellmap_aggr_item
        #     print "sto: TSE, points:", points
        #     print "sto: TSE, coord:", coord

        # rename base_location field's name to 'location' and delete the old key
        agg_row["location"] = agg_row[base_location]
        del agg_row[base_location]

        agg_row["geoloc_simple"] = [
            coord["lat"],
            coord["lon"],
        ]

        data.append(agg_row)

    # _print_wrong_parents(parents)

    print "Entries with borked lan / lon: %s" % "{:,}".format(len(wrong_lat_lon_lines))
    print "Total aggregated lines (mapping_identifier: %s -> base_location: %s): %s" % (mapping_identifier, base_location, "{:,}".format(len(cellmap_aggr.keys())))
    print "Badly-nested entries with multiple parents (omitted): %s" % "{:,}".format(len(aggregated_lines_introducing_multiple_parents))
    print "Accepted cellmap entries: %s" % "{:,}".format(len(data))
    print "Accepted cellmap entries with [0,0] geoloc: %s" % "{:,}".format(default_lat_lon_count)
    print "Invalid lat, lon lines: %s" % "{:,}".format(invalid_lat_lon_lines)
    print "Reverted lat, lon lines: %s" % "{:,}".format(reverted_lat_lon_lines)

    final_base_location_items = _unique_cellmap_column(data, "location")
    print "Accepted unique base location entries: %s" % "{:,}".format(len(final_base_location_items))

    return {
        "data": data,
    }


def create_cellmap(file, column_hierarchy_mapping, cellmap_hierarchy, valid_column_len, delimiter=";", has_header=True):

    data = []
    omitted_wrong_col_len_lines = []
    total_raw_scv_lines = 0

    with open(file, 'r') as fp:
        for i, line in enumerate(fp):
            if has_header and i == 0:
                continue

            total_raw_scv_lines += 1
            row = line.replace("\n", "")
            splitted = row.split(delimiter)
            csv_line = {"line": i, "record": splitted}

            if len(splitted) == valid_column_len:
                record = {}
                for m in column_hierarchy_mapping:
                    record[m] = splitted[column_hierarchy_mapping[m]]
                data.append(record)
            else:
                omitted_wrong_col_len_lines.append(csv_line)

    print "Total CSV lines: %s" % "{:,}".format(total_raw_scv_lines)
    print "CSV Lines with wrong column length (omitted): %s" % "{:,}".format(len(omitted_wrong_col_len_lines))

    base_location = cellmap_hierarchy[-1]
    total_base_location_items = _unique_cellmap_column(data, base_location)
    print "Total (unique) base location entries: %s" % "{:,}".format(len(total_base_location_items))

    return {
        "data": data,
    }


def create_usermap_from_cellmap(cellmap, hierarchy, mapping_identifier):
    base_location = hierarchy[-1]
    data = []

    for c in cellmap:
        data.append({
            "location": c[base_location],
            "identifier": c[mapping_identifier]
        })

    return data

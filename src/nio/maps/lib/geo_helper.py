# NIO_COMPONENT analytics.maps
import os
import math
import subprocess
NODE_MODULES_PATH = os.path.join(os.path.dirname(__file__), '../../../../', 'node_modules')


class Coord(object):
    def __init__(self, lat=-1, lon=-1):
        self.lat = lat
        self.lon = lon

    def __repr__(self):
        return "(Lat: %s, Lon: %s)" % (self.lat, self.lon)

    def getCoord(self):
        return {"lon": self.lon, "lat": self.lat}

    def geojson(self):
        geojson = {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
        }
        return geojson


class ConvexHull(object):
    TURN_LEFT = 1

    def _turn(self, p, q, r):
        return cmp((q[1] - p[1])*(r[0] - p[0]) - (r[1] - p[1])*(q[0] - p[0]), 0)

    def _keep_left(self, hull, r):
        while len(hull) > 1 and self._turn(hull[-2], hull[-1], r) != self.TURN_LEFT:
            hull.pop()
        if not len(hull) or hull[-1] != r:
            hull.append(r)
        return hull

    def convex_hull(self, points):
        if len(points) < 3:
            raise Exception("Cannot calculate shape for less than 3 points")

        points = sorted(points)
        l = reduce(self._keep_left, points, [])
        u = reduce(self._keep_left, reversed(points), [])
        result = l.extend(u[i] for i in xrange(1, len(u) - 1)) or l
        # join last entry to first to create polygon (???)
        if len(set([tuple(p) for p in result])) > 1:
            result.append(result[0])
        else:
            print points
            print result
            raise Exception("Less than required points generated for polygon")
        return result


def calculate_polygon(locations):
    ch = ConvexHull()
    points = [[l.lon, l.lat] for l in locations]
    polygon_points = ch.convex_hull(points)

    return {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": polygon_points
                }
            }
        ]
    }


def flatten(items):
    def is_coordinate_pair(x):
        return len(x) == 2 and isinstance(x[0], (float, int))

    for item in items:
        if is_coordinate_pair(item):
            yield item
        elif isinstance(item, (list, tuple)):
            for subitem in flatten(item):
                yield subitem


def calculate_midpoint(locations):

    def to_radians(x):
        return x * math.pi / 180

    def from_radians(x):
        return x * 180 / math.pi

    x = 0
    y = 0
    z = 0

    for l in locations:
        lat = to_radians(l.lat)
        lon = to_radians(l.lon)

        x += math.cos(lat) * math.cos(lon)
        y += math.cos(lat) * math.sin(lon)
        z += math.sin(lat)

    x = x / len(locations)
    y = y / len(locations)
    z = z / len(locations)

    cLon = math.atan2(y, x)
    cSqr = math.sqrt(x*x + y*y)
    cLat = math.atan2(z, cSqr)

    cLonCoord = from_radians(cLon)
    cLatCoord = from_radians(cLat)

    return Coord(cLatCoord, cLonCoord)


def simplify(geojson, percentage=20):

    cmd = [
        "node",
        "%s/mapshaper/bin/mapshaper" % NODE_MODULES_PATH,
        "-",    # read from stdin
        "auto-snap",
        "encoding=utf8",
        "-simplify",
        "%s%%" % percentage,
        "keep-shapes",
        "-o",
        "format=geojson",
        "-"  # export to stdout
    ]

    proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate(geojson)
    exit_status = proc.returncode

    if exit_status != 0:
        raise Exception("Failed simplifying geojson: %s (exit status: %s)" % (err, exit_status))

    return out


if __name__ == "__main__":

    # Between Kudus and Purwodadi
    locations = [
        Coord(lat=-6.818120, lon=110.845184),
        Coord(lat=-7.093483, lon=110.856171)
    ]
    print calculate_midpoint(locations)

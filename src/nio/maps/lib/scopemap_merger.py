# NIO_COMPONENT analytics.maps

def scopemap_merger(scopemap, non_scopemap, scopes):

    merged_scopemap = {}

    for k in scopemap:
        merged_scopemap[k] = scopemap[k]

    for k in non_scopemap:
        merged_scopemap[k] = non_scopemap[k]

    for scope in scopes["scopes"]:
        for scopemap_key in scope["scopemap_keys"]:
            merged_scopemap["scopes"].append({
                "scope": scopemap_key,
                "cli_key": scope["cli_key"],
                "cellmap_key": scope["cellmap_key"]
            })

    return merged_scopemap

# NIO_COMPONENT analytics.maps
import os
import json
from collections import defaultdict

import geo_helper
import utils_converter

from location import LocationKey
from static import SIMPLIFIED_DIR_TEMPLATE
from static import CENTROID_DIR_TEMPLATE
from static import POLYGON_DIR_TEMPLATE


def save_optimizated_location(geojson_path, geojson, scope_name, location_name, opt_type=None):
    if not os.path.exists(geojson_path):
        print "\tCreating %s directory for scope %s: %s" % (opt_type, scope_name, geojson_path,)
        os.makedirs(geojson_path)

    simplified_geojson_location_path = "%s/%s.json" % (geojson_path, utils_converter.scope_name_to_path(location_name))
    with open(simplified_geojson_location_path, "w") as fp:
        print "\tSaving %s location: %s" % (opt_type, simplified_geojson_location_path,)
        json.dump(json.loads(geojson), fp, indent=4)
        fp.close()


class Optimizer(object):
    def __init__(self, scope_hierarchy, oracle, cellmap, geojson_path, save_optimization):
        self.scope_hierarchy = scope_hierarchy
        self.oracle = oracle
        self.cellmap = cellmap
        self.geojson_path = geojson_path
        self.save_optimization = save_optimization

    def polygon_locations(self, locations_map):
        # handle generation of polygons for locations where we don't have
        # any geojson or coordinates info
        scopes_to_process = {}
        for si in self.scope_hierarchy:
            scope_name = si["scope_name"]
            deduce_polygon = si.get("geojson.deduce_polygon", False)
            if deduce_polygon in ["", False]:
                continue
            print "\tRequested polygons calculation for: %s" % scope_name
            scopes_to_process[scope_name] = {}

        assert "cgi" not in scopes_to_process, "Polygon calculation not supported for cgi level"

        if not scopes_to_process:
            print "\tNo scopes request polygon calculation. Done!"
            return locations_map

        location_to_cells_map = defaultdict(lambda: [])
        for cell_name, c in self.cellmap.items():
            for scope_name in scopes_to_process:
                loc_name = c[scope_name]
                loc_id = self.oracle.get_location_id_from_name(scope_name, loc_name)
                loc_key = LocationKey(scope_name=scope_name, id_=loc_id)
                location_to_cells_map[loc_key].append(c)

        print "\tStarting to find polygons for scopes: %s" % ", ".join(scopes_to_process)

        for loc_key, loc_cells in location_to_cells_map.items():
            points = []
            for cell in loc_cells:
                coordinates = cell.get("geoloc_simple", None) or None
                if not coordinates or len(coordinates) != 2:
                    continue
                p = geo_helper.Coord(lat=coordinates[0], lon=coordinates[1])
                points.append(p)

            if len(set([(po.lon, po.lat) for po in points])) < 3:
                # we need minimally 3 points to create a shape
                # if we have fewer just calculate their centroid
                print "\tNot enough points to create polygon for %s with id: %s" % (loc_key.scope_name, loc_key.id)
                polygon = geo_helper.calculate_midpoint(points).geojson()
            else:
                print loc_key.scope_name, loc_key.id
                polygon = geo_helper.calculate_polygon(points)

            loc = locations_map[loc_key]
            print "\tAdding polygons geojson to location: %s (%s)" % (loc.scope_name, loc.name)
            polygon_geojson = json.dumps(polygon)

            if self.save_optimization:
                polygon_geojson_path = POLYGON_DIR_TEMPLATE % (self.geojson_path, loc_key.scope_name,)
                save_optimizated_location(polygon_geojson_path, polygon_geojson, loc_key.scope_name, loc.name, opt_type="polygon")

            loc.geojson = polygon_geojson

        return locations_map

    def centroid_locations(self, locations_map):
        # handle generation of center points for locations where we don't have
        # any geojson or coordinates info
        scopes_to_centroid = {}
        for si in self.scope_hierarchy:
            scope_name = si["scope_name"]
            deduce_centroid = si.get("geojson.deduce_centroid", False)
            if deduce_centroid in ["", False]:
                continue
            print "\tRequested centroid calculation for: %s" % scope_name
            scopes_to_centroid[scope_name] = {}

        # XXX FIXME we shoulnd't check for "cgi" but for "BASE_LOCATION"
        assert "cgi" not in scopes_to_centroid, "Centroid calculation not supported for cgi level"

        if not scopes_to_centroid:
            print "\tNo scopes need centroid calculation. Done!"
            return locations_map

        location_to_cells_map = defaultdict(lambda: [])
        for cell_name, c in self.cellmap.items():
            for scope_name in scopes_to_centroid:
                loc_name = c[scope_name]
                loc_id = self.oracle.get_location_id_from_name(scope_name, loc_name)
                loc_key = LocationKey(scope_name=scope_name, id_=loc_id)
                location_to_cells_map[loc_key].append(c)

        print "\tStarting to find centroid for scopes: %s" % ", ".join(scopes_to_centroid)

        centroid_geojson_added_count = 0
        centroid_scopes_processed = set()

        for loc_key, loc_cells in location_to_cells_map.items():
            points = []
            for cell in loc_cells:
                coordinates = cell.get("geoloc_simple", None) or None
                if not coordinates or len(coordinates) != 2 or coordinates[0] == 0 or coordinates[1] == 0:
                    continue
                p = geo_helper.Coord(lat=coordinates[0], lon=coordinates[1])
                points.append(p)

            if points:
                centroid = geo_helper.calculate_midpoint(points)
            else:
                # XXX ???
                centroid = geo_helper.Coord(lat=0, lon=0)

            loc = locations_map[loc_key]
            centroid_geojson = json.dumps(centroid.geojson())
            centroid_geojson_added_count += 1
            centroid_scopes_processed.add(loc.scope_name)

            if self.save_optimization:
                centroid_geojson_path = CENTROID_DIR_TEMPLATE % (self.geojson_path, loc_key.scope_name,)
                save_optimizated_location(centroid_geojson_path, centroid_geojson, loc_key.scope_name, loc.name, opt_type="centroid")

            loc.geojson = centroid_geojson

        if centroid_geojson_added_count:
            print "\tAdded centroid geojson to %s locations in %s" % (centroid_geojson_added_count, ", ".join(centroid_scopes_processed))
        return locations_map

    def simplify_locations(self, locations_map):
        # handle locations with complex geojson structures that need to
        # be simplified -- we do this on the fly when creating the new
        # locations update file
        scopes_to_simplify = {}
        for si in self.scope_hierarchy:
            scope_name = si["scope_name"]
            simplification_level = si.get("geojson.simplification_level", 100)
            if simplification_level in ["", None, 100]:
                continue
            scopes_to_simplify[scope_name] = simplification_level

        if not scopes_to_simplify:
            print "\tNo scopes need geojson simplification. Done!"
            return locations_map

        print "Starting to simplify scopes: %s" % ", ".join(scopes_to_simplify.keys())

        for loc_key, loc in locations_map.items():
            if loc_key.scope_name not in scopes_to_simplify:
                continue
            simplification_level = scopes_to_simplify[loc_key.scope_name]

            geojson = loc.geojson
            if geojson in [None, ""]:
                continue
            try:
                if type(geojson) not in [str, unicode]:
                    geojson = json.dumps(geojson)
                    print "invalid geojson type:", type(geojson)
                simplified_geojson = geo_helper.simplify(geojson, percentage=simplification_level)

                if self.save_optimization:
                    simplified_geojson_path = SIMPLIFIED_DIR_TEMPLATE % (self.geojson_path, loc_key.scope_name, simplification_level,)
                    save_optimizated_location(simplified_geojson_path, simplified_geojson, loc_key.scope_name, loc.name, opt_type="simplify")

                print "\tSimplified geojson for %s from %s to %s" % (loc.name, len(geojson), len(simplified_geojson))
                locations_map[loc_key].geojson = simplified_geojson
            except Exception as e:
                print "\tCouldn't simplify %s: %s geojson -- %s" % (loc_key.scope_name, loc.name, e)

        return locations_map

# NIO_COMPONENT analytics.maps
class ProgessPercentage(object):
    def __init__(self, message, total, round_to_tens=True):
        self.message = message
        self.total = total
        self.prev_percentage_done = None
        self.round_to_tens = round_to_tens

    def print_progress(self, i):
        if i == self.total:
            raise Exception("More items than total count reported for progress display")
        percentage_done = ((i + 1) * 100) / self.total

        if self.round_to_tens:
            percentage_done = ((percentage_done * 100) / 1000) * 10

        if percentage_done != self.prev_percentage_done and percentage_done % 20 == 0:
            item = 0 if percentage_done == 0 else i + 1
            print self.message + " - Done: %(percentage_done)s%% (#%(item)s)" % {"percentage_done": percentage_done, "item": item}
            self.prev_percentage_done = percentage_done

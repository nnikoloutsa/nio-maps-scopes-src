# NIO_COMPONENT analytics.maps

from static import NOT_AVAILABLE_LOCATION


def _get_pois_gis(config_reader, cgi_to_coordinates):

    dedup_set = set()
    pois_gis = []
    for obj in config_reader.pois_info:
        cgis_list = obj["locations"]
        mpoints = set()
        cgis_not_found_in_cellmap = set()
        for cgi in cgis_list:
            if cgi_to_coordinates.get(cgi):
                mpoints.add(cgi_to_coordinates[cgi])
            else:
                cgis_not_found_in_cellmap.add(cgi)

        poi_name = obj["name"]

        if cgis_not_found_in_cellmap:
            print "POI %s: %s cgis not found in cellmap: %s" % (poi_name, len(cgis_not_found_in_cellmap), ", ".join(cgis_not_found_in_cellmap))
        
        long_lat = (obj["lon"], obj["lat"])

        if poi_name in dedup_set:
            continue

        pois_gis.append({
            "scope_name": "poi",
            "scope_value": poi_name,
            "cgis_list": cgis_list,
            "mpoint": get_wkt_by_geo_type("MULTIPOINT", list(mpoints)) if mpoints else None,
            "geom": get_point_wkt(long_lat)
        })

        dedup_set.add(poi_name)

    return pois_gis


def _get_countries_gis(config_reader):
    countries_gis = []
    dedup_set = set()
    for ci in config_reader.countries:
        country_name = ci["country"]
        geojson = ci.get("geojson", None)
        if not geojson:
            continue
        if country_name in dedup_set:
            print "country: %s already in set skipping" % country_name
            continue
        countries_gis.append({
            "scope_name": "country",
            "scope_value": country_name,
            "cgis_list": [],
            "mpoint": None,
            "geojson": geojson
        })
        dedup_set.add(country_name)

    return countries_gis


def get_point_wkt(coordinates_list):
    return 'POINT(%s)' % ' '.join(map(str, coordinates_list))


def get_wkt_by_geo_type(geo_type, coordinates_list):
    return '%s(' % geo_type + ', '.join(
        map(
            lambda s: ' '.join(
                map(str, s)
            ), coordinates_list
        )
    ) + ')'


def get_locations_gis(config_reader):
    locations_gis = []
    locations_map = {}
    cgi_to_coordinates = {}

    cellmap_to_scopemap_key_mapping = {
        si["cellmap_key"]: {
            "scope_name": si["scope_name"],
            "simplification_level": si.get("geojson.simplification_level", None),
            "geojson.deduce_centroid": si.get("geojson.deduce_centroid", False),
            "geojson.deduce_polygon": si.get("geojson.deduce_polygon", False)
        } for si in config_reader.scopes_info["scopes"]
    }

    dedup_set = set()
    for cell_obj in config_reader.cellmap.itervalues():
        cgi = cell_obj["location"]
        rat = cell_obj.get("rat", "")
        # In mapping frameworks spatial coordinates are often in order of latitude and longitude.
        # In spatial databases spatial coordinates are in x = longitude, and y = latitude.
        # https://postgis.net/2013/08/18/tip_lon_lat/
        long_lat = tuple(cell_obj['geoloc_simple'][::-1])
        cgi_to_coordinates[cgi] = long_lat

        if cgi not in dedup_set:
            locations_gis.append({
                "scope_name": "cgi",
                "scope_value": cgi,
                "rat": rat,
                "cgis_list": [cgi],
                "mpoint": get_wkt_by_geo_type("MULTIPOINT", [long_lat]),
                "geom": get_point_wkt(long_lat)
            })

        dedup_set.add(cgi)

        for location in cellmap_to_scopemap_key_mapping:
            if cell_obj.get(location):
                if cell_obj[location] == NOT_AVAILABLE_LOCATION:
                    continue
                key = (cellmap_to_scopemap_key_mapping[location]["scope_name"], cell_obj[location])

                if key not in locations_map:
                    locations_map[key] = {
                        "cgis_list": set(),
                        "cgis_points": set(),
                        "simplification_level": cellmap_to_scopemap_key_mapping[location]["simplification_level"],
                        "geojson.deduce_centroid": cellmap_to_scopemap_key_mapping[location]["geojson.deduce_centroid"],
                        "geojson.deduce_polygon": cellmap_to_scopemap_key_mapping[location]["geojson.deduce_polygon"]
                    }

                locations_map[key]["cgis_list"].add(cgi)
                locations_map[key]["cgis_points"].add(long_lat)

    dedup_set = set()
    for k, v in locations_map.iteritems():
        if k in dedup_set:
            continue
        scope_name = k[0]
        scope_value = k[1]
        geojson = None
        if scope_name in config_reader.scope_name_id_map:
            if scope_value in config_reader.scope_name_id_map[scope_name]:
                _id = config_reader.scope_name_id_map[scope_name][scope_value]
                geojson = config_reader.get_geojson_for_location(scope_name, _id)
        locations_gis.append({
            "scope_name": k[0],
            "scope_value": k[1],
            "cgis_list": list(v["cgis_list"]),
            "mpoint": get_wkt_by_geo_type("MULTIPOINT", list(v["cgis_points"])),
            "simplification_level": v["simplification_level"],
            "geojson.deduce_centroid": v["geojson.deduce_centroid"],
            "geojson.deduce_polygon": v["geojson.deduce_polygon"],
            "geojson": geojson
        })

        dedup_set.add(k)

    geojson = config_reader.get_geojson_for_location("nation", -1)
    locations_gis.append({
        "scope_name": "nation",
        "scope_value": config_reader.scopes_info["meta"].get("nation", "nation"),
        "cgis_list": [],
        "mpoint": None,
        "geojson": geojson
    })

    countries_gis = _get_countries_gis(config_reader)

    locations_gis += countries_gis

    pois_gis = _get_pois_gis(config_reader, cgi_to_coordinates)

    locations_gis += pois_gis

    return locations_gis

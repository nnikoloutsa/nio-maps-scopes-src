# NIO_COMPONENT analytics.maps
import os
import json
import copy

from static import DNA_SCOPE_AGGREGATION_KEYS
from static import NOT_AVAILABLE_LOCATION
from static import SIMPLIFIED_DIR_TEMPLATE
from static import CENTROID_DIR_TEMPLATE
from static import POLYGON_DIR_TEMPLATE
from static import ROOT_SCOPE_ID
from static import ROOT_SCOPE_NAME
from static import BASE_SCOPE_NAME

from geo_helper import Coord
from utils_converter import scope_path_to_name
from location import LocationKey
from helper import ProgessPercentage


PRIORITY_APPS_LIMIT = 5


class ConfigReader(object):
    # the scopes_info field used to determine
    # the name of the scope in cellmap
    # we use cli_key for now because the old scopemap
    # used that, and nlive expects those names
    SCOPEMAP_KEY = "cli_key"
    CELLMAP_KEY = "cellmap_key"

    def __init__(self, config_path, geojson_path, countries_json_path, countries_geojson_dir, save_optimization):
        self.logger_store = {}
        self.config_path = config_path
        self.geojson_path = geojson_path
        self.countries_json_path = countries_json_path
        self.countries_geojson_dir = countries_geojson_dir
        self.save_optimization = save_optimization

        self.scopes_info = self.read_scopes_file()
        self.cellmap, self.cellmap_raw = self.read_cellmap_file_dict(self.scopes_info)

        self.scopemap = ConfigReader._generate_scopemap(self.scopes_info, self.cellmap)
        self.scope_name_id_map = self._generate_scope_name_id_map(self.scopes_info, self.scopemap)

        self.poi2cell, self.pois_info = self.read_pois_file()

        self.location_to_geojson_map = self.read_location_to_geojson_map(self.scope_name_id_map)
        self.countries = self.read_countries()

        self.non_scopemap = self.read_non_scopemap()

        self.analytics_id_groups = self.read_analytics_id_groups()
        self.nlive_aggregations = self.read_nlive_aggregations()

        class Oracle(object):
            pass
        oracle = Oracle()
        oracle.get_geojson_for_location = self.get_geojson_for_location
        oracle.get_location_id_from_name = self.get_location_id_from_name
        self.oracle = oracle

    def get_key_apps(self):
        try:
            if not self.analytics_id_groups or "key_apps" not in self.analytics_id_groups:
                print "analytics_id_groups not found (will default to empty key_apps.txt file)"
                return []

            key_apps = copy.copy(self.analytics_id_groups["key_apps"]["values"])
            dim = self.analytics_id_groups["key_apps"]["dimension"]
            if dim != "app":
                raise Exception('Expected dimension of app for analytics_id_groups key_apps!')
            key_apps = self.analytics_id_groups["key_apps"]["values"]
            if not isinstance(key_apps, list):
                raise Exception('Expected type list for analytics_id_groups key_apps!')
            key_apps = [str(i) for i in key_apps]
        except Exception as e:
            raise Exception("analytics_id_groups.json has invalid formatting: %s" % e)

        return key_apps

    def get_priority_apps(self):
        try:
            if not self.analytics_id_groups or "priority_apps" not in self.analytics_id_groups:
                print "analytics_id_groups not found (will default to empty priority_apps.txt file)"
                return []

            priority_apps = copy.copy(self.analytics_id_groups["priority_apps"]["values"])
            dim = self.analytics_id_groups["priority_apps"]["dimension"]
            if dim != "app":
                raise Exception('Expected dimension of app for analytics_id_groups priority_apps!')
            priority_apps = self.analytics_id_groups["priority_apps"]["values"]
            if not isinstance(priority_apps, list):
                raise Exception('Expected type list for analytics_id_groups priority_apps!')
            priority_apps = [str(i) for i in priority_apps]
        except Exception as e:
            raise Exception("analytics_id_groups.json has invalid formatting: %s" % e)

        if len(priority_apps) > PRIORITY_APPS_LIMIT:
            print "Number of priority_apps: %d greater than limit: %d - Return first %d apps" % (len(priority_apps), PRIORITY_APPS_LIMIT, PRIORITY_APPS_LIMIT)

        return priority_apps[:PRIORITY_APPS_LIMIT]

    def get_scopes_active(self):
        scope_keys = []
        for s in self.scopes_info["scopes"]:
            if "analytics_key" in s and s["analytics_key"].startswith("scope_"):

                aggregations = []
                if "aggregations" in s:
                    if not set(DNA_SCOPE_AGGREGATION_KEYS).issuperset(set(s["aggregations"])):
                        print set(DNA_SCOPE_AGGREGATION_KEYS) - set(s["aggregations"])
                        raise Exception("DNA_SCOPE_AGGREGATION_KEYS are not superset of aggregations for scope_name: %s" % s["scope_name"])
                    aggregations = s["aggregations"]

                _aggregations = [agg.replace("scope_x", s["analytics_key"]) for agg in aggregations]

                scope_keys.append({
                    "scope_key": s["analytics_key"],
                    "cellmap_key": s["cellmap_key"],
                    "aggregations": _aggregations,
                })

        return scope_keys

    def get_cellmap_scopes(self):
        scope_keys = {
            "location": "location"
        }
        for s in self.scopes_info["scopes"]:
            if "analytics_key" in s and s["analytics_key"].startswith("scope_"):
                scope_keys[s["analytics_key"]] = s["cellmap_key"]

        data = []
        for c in self.cellmap_raw:
            row = {}
            for key, value in scope_keys.items():
                row[key] = c[value]
            data.append(row)

        return data

    def ger_errors(self):
        errors = []
        for log_type, log_subtypes in self.logger_store.items():
            for log_subtype, log_messages in log_subtypes.items():
                errors.append("%s/%s: %s" % (log_type, log_subtype, len(log_messages)))

        return errors

    def print_errors(self):
        for error in self.ger_errors():
            print "%s" % error

    def logger(self, log_type, message, log_subtype="generic"):
        if log_type not in self.logger_store:
            self.logger_store[log_type] = {}

        if log_subtype not in self.logger_store[log_type]:
            self.logger_store[log_type][log_subtype] = []

        self.logger_store[log_type][log_subtype].append(message)

        print message

    def read_analytics_id_groups(self):
        print "Reading analytics_id_groups..."

        analytics_id_groups = {}
        if os.path.exists("%s/analytics_id_groups.json" % self.config_path):
            with open("%s/analytics_id_groups.json" % self.config_path, "r") as fp:
                analytics_id_groups = json.load(fp)

        return analytics_id_groups

    def read_nlive_aggregations(self):
        print "Reading nlive_aggregations..."

        nlive_aggregations = {}
        if os.path.exists("%s/nlive_aggregations.json" % self.config_path):
            with open("%s/nlive_aggregations.json" % self.config_path, "r") as fp:
                nlive_aggregations = json.load(fp)

            for agg_id, agg_config in nlive_aggregations.items():
                if "filters" not in agg_config:
                    continue

                if "dimensions" not in agg_config["filters"]:
                    print "No dimensions:", agg_config

                for dim_config in agg_config["filters"]["dimensions"]:
                    agg_value = dim_config["values"]
                    if not isinstance(agg_value, list):
                        if agg_value not in self.analytics_id_groups:
                            raise Exception('Aggregation value not in analytics_id_groups!')

                        dim_config["values"] = [str(v) for v in self.analytics_id_groups[agg_value]["values"]]

        return nlive_aggregations

    def read_non_scopemap(self):
        print "Reading non-scopemap..."

        with open("%s/non-scopemap.json" % self.config_path, "r") as fp:
            non_scopemap = json.load(fp)

        # validation: operator_group should not be multimap
        if non_scopemap.get("operator_group"):
            op_grp = non_scopemap["operator_group"]
            s = set()
            for grp in op_grp:
                cur_plmns = set(grp["plmns"])
                # check cur_plms against s (intersection)
                common_plmns = s & cur_plmns
                if common_plmns:
                    raise Exception("Found overlapping plmns: %s in operator_group: %s" % (str(common_plmns), grp["name"]))
                # add cur_plmns in s and check next operator group (union)
                s |= cur_plmns

        return non_scopemap

    def read_pois_file(self):
        print "Reading POIs..."

        with open("%s/pois.json" % self.config_path, "r") as fp:
            pois = json.load(fp)
        with open("%s/poi2cell.json" % self.config_path, "r") as fp:
            poi_2_cell = json.load(fp)

        max_pois_per_cgi = int(
            self.scopes_info
                .get("meta", {})
                .get("max_pois_per_cgi", 0)
            )

        poi_dict = {}
        cgi_dict = {}
        for row in poi_2_cell:
            cgi = row["cgi"]
            lpois = row["pois"]
            if cgi not in cgi_dict:
                cgi_dict[cgi] = list()
            ls = cgi_dict[cgi]
            for poi_id in lpois:
                n = len(ls)
                if not max_pois_per_cgi or n < max_pois_per_cgi:
                    if poi_id not in ls:
                        ls.append(poi_id)
                    if poi_id not in poi_dict:
                        poi_dict[poi_id] = list()
                    poi_dict[poi_id].append(cgi)
                else:
                    logger_dict = dict(
                        bsn = BASE_SCOPE_NAME,
                        val = cgi,
                        poi = poi_id,
                        limit = max_pois_per_cgi
                    )
                    # self.logger(
                    #     "POI LIMIT HIT FOR %(bsn)s" % logger_dict,
                    #     "\tPOI limit(%(limit)s) hit for %(bsn)s: %(val)s, poi: %(poi)s" % logger_dict
                    # )

        # emit filtered poi2cell
        poi2cell2 = []
        for row in poi_2_cell:
           cgi = row["cgi"]
           lpois = row["pois"]
           cgi_pois = cgi_dict.get(cgi, [])
           lpois2 = []
           for poi in lpois:
               if poi in cgi_pois:
                   lpois2.append(poi)
           if not lpois2:
               continue
           row2 = dict(cgi = cgi, pois = lpois2)
           poi2cell2.append(row2)

        data = []

        for idx, poi in enumerate(pois):
            if poi.get("geoloc_simple", None) in ["", {}, None]:
                self.logger("POI_WITHOUT_GEOLOC", "\tPOI lacks geoloc_simple information - using [0,0]: %s " % poi)
                poi["geoloc_simple"] = [0, 0]

            lat = poi.get("geoloc_simple")[0]
            lon = poi.get("geoloc_simple")[1]

            poi_row = {
                "id": poi["id"],
                "name": poi["name"],
                "geojson": json.dumps(Coord(lat=lat, lon=lon).geojson()),
                "lon": lon,
                "lat": lat,
                "locations": [],
            }

            if poi["id"] in poi_dict:
                poi_row["locations"] = poi_dict[poi["id"]]
            else:
                self.logger("POI_WITHOUT_BASE_LOCATION", "\tPOI without %s: %s" % (BASE_SCOPE_NAME, poi))

            data.append(poi_row)

        print "\t* Loaded POIs (%s)" % len(pois)

        return poi2cell2, data

    def read_scopes_file(self):
        print "Reading scopes..."

        with open("%s/scopes.json" % self.config_path, "r") as fp:
            scopes_info = json.load(fp)

        print "\t* Loaded scopes (%s)" % (len(scopes_info["scopes"]))
        return scopes_info

    def read_cellmap_file_dict(self, scopes_info):
        print "Reading cellmap..."

        scope_name_to_cellmap_key_map = {}
        for si in scopes_info["scopes"]:
            scope_name = si["scope_name"]
            cellmap_key = si["cellmap_key"]
            if scope_name != cellmap_key:
                scope_name_to_cellmap_key_map[scope_name] = cellmap_key

        cellmap_file = scopes_info["cellmap_file"] if "cellmap_file" in scopes_info else "cellmap.json"
        CELLMAP_PATH = "%s/%s" % (self.config_path, cellmap_file,)

        mandatory_keys_set = {"location", "geoloc_simple"}

        cellmap = {}
        cellmap_raw = []
        cellmap_file = []

        scope_entries_without_location_keys = {}

        with open(CELLMAP_PATH, "r") as fp:
            cellmap_file = json.load(fp)

        cellmap_entries_missing_keys = set()

        for c in cellmap_file:
            c_raw = copy.deepcopy(c)
            # enrich cell with missing entries for scope_name
            # so we don't have to translate between it and cellmap_key
            for scope_name, cellmap_key in scope_name_to_cellmap_key_map.items():
                if scope_name not in c:
                    c[scope_name] = c[cellmap_key]

            cell_keys_set = set(c.keys())

            if not cell_keys_set.issuperset(mandatory_keys_set):
                # ignore cells without all mandatory keys
                cellmap_entries_missing_keys.add(c.get("location", "-"))
                continue

            for scope in scopes_info["scopes"]:
                if bool(c.get(scope["scope_name"])) is False:
                    scope_name = scope["scope_name"]
                    if scope_name not in scope_entries_without_location_keys:
                        scope_entries_without_location_keys[scope_name] = 0
                    scope_entries_without_location_keys[scope_name] += 1

                    c[scope_name] = NOT_AVAILABLE_LOCATION
                    c[scope["cellmap_key"]] = NOT_AVAILABLE_LOCATION
                    c_raw[scope["cellmap_key"]] = NOT_AVAILABLE_LOCATION

            cellmap[c["location"]] = c
            cellmap_raw.append(c_raw)

        if len(cellmap) == 0 and len(cellmap_file) != 0:
            raise Exception("Zero sized cellmap!")

        if cellmap_entries_missing_keys:
            self.logger("CELLMAP_ENTRY_WITHOUT_MANDATORY_KEYS", "\tMandatory keys: %s not present in all cellmap entries" % ", ".join(mandatory_keys_set))
            self.logger("CELLMAP_ENTRY_WITHOUT_MANDATORY_KEYS", "\tOmitted %s cellmap entries without mandatory keys." % (len(cellmap_entries_missing_keys),))
            # self.logger("CELLMAP_ENTRY_WITHOUT_MANDATORY_KEYS", "\tOmitted cellmap entries missing mandatory keys: %s" % (", ".join(cellmap_entries_missing_keys),))

        for scope, entries_without_location_keys_count in scope_entries_without_location_keys.iteritems():
            if entries_without_location_keys_count > 0:
                self.logger(
                    "CELLMAP_ENTRY_WITHOUT_LOCATION_KEYS",
                    "\t Scope_name: %s missing %s keys - using %s" % (scope, entries_without_location_keys_count, NOT_AVAILABLE_LOCATION),
                    log_subtype=scope
                )

        print "\t* Loaded cellmap (%s)" % len(cellmap)
        return cellmap, cellmap_raw

    @staticmethod
    def _generate_scopemap(scopes_info, cellmap):
        print "Generating scopemap..."
        scopemap = {}
        scope_locations_set_map = {}

        # prepare empty lists in cellmap dict for each scope
        # we're gonna load locations into
        for si in scopes_info["scopes"]:
            scopemap_key = si[ConfigReader.SCOPEMAP_KEY]
            scopemap[scopemap_key] = []
            # map of sets per scope to keep track of
            # which locations we have already seen
            # in previous cells to avoid multiple entries
            scope_locations_set_map[scopemap_key] = set()

        # go through all cells, read the location for each scope
        # and copy it to scopemap list with that scope key
        # assigning increasing id numbers for each scope-location pair
        for cell in cellmap.values():
            for si in scopes_info["scopes"]:
                cellmap_key = si[ConfigReader.CELLMAP_KEY]
                scopemap_key = si[ConfigReader.SCOPEMAP_KEY]
                location_name = cell.get(cellmap_key, None)

                if location_name is None:
                    raise Exception("Cell %s is missing scope entry: %s" % (cell.get("location"), cellmap_key))

                if location_name in scope_locations_set_map[scopemap_key]:
                    # don't add an already added location
                    continue

                location_entry = {
                    "name": location_name,
                    "id": len(scopemap[scopemap_key]) + 1
                }

                if "copy_to_scopemap" in si:
                    for attr in si["copy_to_scopemap"]:
                        value = cell.get(attr, None)
                        if value is None:
                            raise Exception("Cell %s is missing scope entry: %s" % (cell.get("location"), attr))

                        location_entry[attr] = value

                scopemap[scopemap_key].append(location_entry)
                scope_locations_set_map[scopemap_key].add(location_name)

        print "\t* Generated scopemap (%s)" % len(scopemap)
        return scopemap

    def _generate_scope_name_id_map(self, scopes_info, scopemap):
        scopemap_key_to_scope_name_map = {si[ConfigReader.SCOPEMAP_KEY]: si["scope_name"] for si in scopes_info["scopes"]}

        scope_name_id_map = {}

        for scopemap_key, scope_locations in scopemap.items():
            scope_name = scopemap_key_to_scope_name_map[scopemap_key]
            scope_name_id_map[scope_name] = {}
            for l in scope_locations:
                scope_name_id_map[scope_name][l["name"]] = l["id"]

        return scope_name_id_map

    def read_location_to_geojson_map(self, scope_name_id_map):
        print "Reading geojson..."

        location_to_geojson_map = {}

        scopes_geojson = self.scopes_info["scopes"] + [
            {
                "scope_name": ROOT_SCOPE_NAME
            }
        ]

        for scope in scopes_geojson:
            scope_name = scope["scope_name"]

            scope_path = "%s%s" % (self.geojson_path, scope_name,)
            if self.save_optimization is False:
                if "geojson.simplification_level" in scope:
                    scope_simplify_path = SIMPLIFIED_DIR_TEMPLATE % (self.geojson_path, scope_name, scope["geojson.simplification_level"])
                    if os.path.exists(scope_simplify_path):
                        scope_path = scope_simplify_path
                        del scope["geojson.simplification_level"]

                elif "geojson.deduce_centroid" in scope:
                    scope_centroid_path = CENTROID_DIR_TEMPLATE % (self.geojson_path, scope_name,)
                    if os.path.exists(scope_centroid_path):
                        scope_path = scope_centroid_path
                        del scope["geojson.deduce_centroid"]

                elif "geojson.deduce_polygon" in scope:
                    scope_polygon_path = POLYGON_DIR_TEMPLATE % (self.geojson_path, scope_name,)
                    if os.path.exists(scope_polygon_path):
                        scope_path = scope_polygon_path
                        del scope["geojson.deduce_polygon"]

            if not os.path.exists(scope_path):
                has_deduce_option = scope.get("geojson.deduce_centroid") or scope.get("geojson.deduce_polygon")
                if has_deduce_option:
                    # legit code path, we will calucalte the geojson from the base locations that belong to the dimension
                    pass
                else:
                    # not legit code path, geojson directory not found but needed
                    self.logger("GEOJSON_DIR_NOT_FOUND", "Geojson directory for %s not found" % scope_name, log_subtype=scope_name)
                continue

            name_id_map = scope_name_id_map.get(scope_name, {})
            # XXX needed to match lowercase names in geojson folder
            lowercase_name_id_map = {k.lower(): v for k, v in name_id_map.items()}

            list_dir = os.listdir(scope_path)
            non_id_locations = []
            pp = ProgessPercentage("\tLoading %s geojson folder - Scope: %s" % (scope_path, scope_name,), len(list_dir))
            for idx, location_file in enumerate(list_dir):
                if location_file.startswith("."):
                    continue

                # assumes the customer/geojson/<scope_name>/<location_id>.<location_name>.json filename
                # but discards the not 100% trusted <location_id> and uses the scopemap one
                location_name = scope_path_to_name(location_file[:location_file.find(".json")])
                # print location_name
                if scope_name == ROOT_SCOPE_NAME:
                    # the root_scope (e.g. nation) is not on the scopemap file
                    # so we need handled its id as an exception
                    location_id = ROOT_SCOPE_ID
                else:
                    location_id = lowercase_name_id_map.get(location_name.lower(), None)

                if location_id is None:
                    non_id_locations.append("\tscope: %s, location: %s" % (scope_name, location_name))
                    continue

                with open(scope_path + "/" + location_file, 'r') as fp:
                    geojson = fp.read()

                key = LocationKey(scope_name=scope_name, id_=location_id)
                location_to_geojson_map[key] = geojson

                pp.print_progress(idx)

            if non_id_locations:
                # print "\t- Couldn't find id for:"
                # for l in non_id_locations:
                #     self.logger("GEOJSON_IDS_NOT_FOUND", "\t%s" % l, log_subtype=scope_name)
                print "\t- Couldn't find id for %s locations in scope: %s" % (len(non_id_locations), scope_name)

        print "\t* Loaded geojson (%s)" % len(location_to_geojson_map)
        return location_to_geojson_map

    def read_countries(self):
        print "Reading countries..."

        countries_info = []
        with open(self.countries_json_path, "r") as fp:
            countries_info = json.load(fp)

        geojson_without_country_info = []
        country_code_to_country_map = {c["iso_alpha_3_code"]: c for c in countries_info}

        for filename in os.listdir(self.countries_geojson_dir):
            if not filename.endswith(".json"):
                continue
            country_iso_alpha_3_code = filename[:3]
            with open(self.countries_geojson_dir + "/" + filename, "r") as fp:
                geojson = json.load(fp)

            if country_iso_alpha_3_code not in country_code_to_country_map:
                geojson_without_country_info.append(country_iso_alpha_3_code)
            else:
                country_code_to_country_map[country_iso_alpha_3_code]["geojson"] = geojson

        if len(geojson_without_country_info):
            chunk_size = 10
            chunks = (geojson_without_country_info[i:i + chunk_size] for i in range(0, len(geojson_without_country_info), chunk_size))
            print "\tCountry-level geojson without country information:"
            for c in chunks:
                print "\t\t%s" % ", ".join(c)

        countries = country_code_to_country_map.values()

        countries_without_geojson = [c["country"] for c in countries if c.get("geojson", None) is None]
        if len(countries_without_geojson):
            print "\tCountries without geojson:"
            for c in countries_without_geojson:
                print "\t\t%s" % c

        print "\t* Loaded countries (%s)" % len(countries)
        return countries

    def get_geojson_for_location(self, scope_name, id_):
        loc_key = LocationKey(scope_name=scope_name, id_=id_)
        return self.location_to_geojson_map.get(loc_key, "")

    def get_location_id_from_name(self, scope_name, name):
        if scope_name == ROOT_SCOPE_NAME:
            return ROOT_SCOPE_ID
        scope_entries = self.scope_name_id_map.get(scope_name, None)
        if scope_entries is None:
            raise Exception("No such scope found: %s" % scope_name)
        entry = scope_entries.get(name, None)
        if entry is None:
            raise Exception("No such location name found %s - %s" % (scope_name, name))
        return entry

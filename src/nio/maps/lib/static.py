# NIO_COMPONENT analytics.maps
ROOT_PARENT_SCOPE_NAME = "overall"
ROOT_SCOPE_ID = -1
ROOT_SCOPE_NAME = "nation"
BASE_SCOPE_NAME = "cgi"

SIMPLIFIED_DIR_TEMPLATE = "%s%s.level.%s"
CENTROID_DIR_TEMPLATE = "%s%s.centroid"
POLYGON_DIR_TEMPLATE = "%s%s.polygon"

NOT_AVAILABLE_LOCATION = "N/A location"

DNA_SCOPE_AGGREGATION_KEYS = [
    # generic
    "scope_x",
    "scope_x.rat",
    "scope_x.browser",
    "scope_x.app_category",
    "scope_x.app.app_category",
    "scope_x.device.device_vendor",
    # vsa
    "scope_x.vsa",
    "scope_x.vsa.rat",
    # fraud
    "scope_x.fraud.destination_server_ip.port.owner",
    "scope_x.fraud.domain.destination_server_ip.port.owner",
    "scope_x.fraud.domain.msisdn.destination_server_ip.port.owner",
    "scope_x.fraud.domain.msisdn.destination_server_ip.port.owner.is_new",
    # snifraud
    "scope_x.snifraud.destination_server_ip.port.owner",
    "scope_x.snifraud.domain.destination_server_ip.port.owner",
    "scope_x.snifraud.domain.msisdn.destination_server_ip.port.owner.is_new",
    # dci
    "scope_x.dci",
]

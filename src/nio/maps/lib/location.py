# NIO_COMPONENT analytics.maps
class LocationKey(object):
    def __init__(self, scope_name, id_):
        self.scope_name = scope_name
        self.id = id_

    def __hash__(self):
        return hash((self.scope_name, self.id))

    def __eq__(self, other):
        return (self.scope_name, self.id) == (other.scope_name, other.id)

    def __ne__(self, other):
        return not(self == other)


class LocationInfo(object):
    def __init__(self, scope_name, analytics_key, parent_analytics_key, id_, name, parent_scope_name, parent_id, coordinates, geojson, mcc, lon, lat, left, right):
        self.scope_name = scope_name
        self.analytics_key = analytics_key
        self.parent_analytics_key = parent_analytics_key
        self.id = id_
        self.name = name
        self.parent_scope_name = parent_scope_name
        self.parent_id = parent_id
        self.coordinates = coordinates
        self.geojson = geojson
        self.mcc = mcc
        self.lon = lon
        self.lat = lat
        self.left = left
        self.right = right

    def _asdict(self):
        return {
            "scope": self.scope_name,
            "analytics_key": self.analytics_key,
            "parent_analytics_key": self.parent_analytics_key,
            "id": self.id,
            "name": self.name,
            "parent_scope": self.parent_scope_name,
            "parent_id": self.parent_id,
            "coordinates": self.coordinates,
            "geojson": self.geojson,
            "mcc": self.mcc,
            "lon": self.lon,
            "lat": self.lat,
            "left": self.left,
            "right": self.right,
        }

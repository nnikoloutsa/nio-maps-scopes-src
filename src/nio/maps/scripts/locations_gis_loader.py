#!/usr/bin/env python
# NIO_COMPONENT analytics.maps

import os
import sys
import simplejson
import psycopg2
import time

from argparse import ArgumentParser

from niolib.database.pg_helpers import PgHelper
from niolib.readers.credentials_reader import CredentialsReader
from niolib.database.pg_helpers import PgConf


from niolib.loggers.script_logger import ScriptLogger

CHUNK_SIZE = 10000
CRED_FILENAME = "/etc/opt/nio/credentials.json"
CRED_KEYFILE = "/etc/opt/nio/credentials.key"


class DbConf(PgConf):
    def set_config(self, database, username, password, hostname, port):
        self.database = database
        self.username = username
        self.password = password
        self.hostname = hostname
        self.port = port


def get_pg_conf(database="pg_analytics"):
    cred = CredentialsReader(filename=CRED_FILENAME, keyfile=CRED_KEYFILE)
    db_cred = cred.get_all().get("pg_analytics")
    db_conf = DbConf(
        database=db_cred.get("database"),
        username=db_cred.get("username"),
        password=db_cred.get("password"),
        hostname=db_cred.get("hostname"),
        port=db_cred.get("port")
    )

    return db_conf


STAGING_TMP_TABLE = """
    CREATE TEMP TABLE IF NOT EXISTS locations_gis_staging(
        scope_name TEXT NOT NULL,
        scope_value TEXT NOT NULL,
        rat TEXT DEFAULT '',
        cgis_list TEXT[],
        qtype TEXT NOT NULL,
        g_mpoint geometry(GEOMETRY, {srid}),
        g_column geometry(GEOMETRY, {srid})
    )
"""

RENAME_TMP_TABLE = """
    ALTER TABLE locations_gis_tmp RENAME TO locations_gis
"""

DUPLICATE_PROD_TABLE = """
    CREATE TABLE locations_gis_tmp (LIKE locations_gis INCLUDING ALL)
"""

DROP_TABLE = """
    DROP TABLE locations_gis
"""

LOCATIONS_TABLE = """
    CREATE TABLE IF NOT EXISTS locations_gis(
        scope_name TEXT NOT NULL,
        scope_value TEXT NOT NULL,
        rat TEXT DEFAULT '',
        cgis_list TEXT[],
        g_mpoint geometry(GEOMETRY, {srid}),
        g_column geometry(GEOMETRY, {srid}),
        CONSTRAINT locations_gis_pk PRIMARY KEY (scope_name, scope_value)
    )
"""

GEOMETRY_COLUMN_INDEX = """
    CREATE INDEX locations_gis_idx ON locations_gis USING GIST (g_column)
"""

MPOINT_COLUMN_INDEX = """
    CREATE INDEX locations_gis_mpoint_idx ON locations_gis USING GIST (g_mpoint)
"""

SQL_CGI = """
    INSERT INTO locations_gis_tmp (scope_name, scope_value, rat, cgis_list, g_mpoint, g_column)
    SELECT
        scope_name,
        scope_value,
        rat,
        cgis_list,
        ST_Union(g_mpoint),
        ST_Union(g_column)
    FROM locations_gis_staging
    WHERE qtype = 'cgi'
    GROUP BY scope_name, scope_value, rat, cgis_list
"""


SQL_DEDUCED_CENTROID = """
    INSERT INTO locations_gis_tmp (scope_name, scope_value, rat, cgis_list, g_mpoint, g_column)
    SELECT
        scope_name,
        scope_value,
        rat,
        cgis_list,
        ST_Union(g_mpoint),
        ST_CENTROID(ST_Union(g_column))
    FROM locations_gis_staging
    WHERE qtype = 'deduce_centroid'
    GROUP BY scope_name, scope_value, rat, cgis_list
"""

SQL_DEDUCED_POLYGON = """
    INSERT INTO locations_gis_tmp (scope_name, scope_value, rat, cgis_list, g_mpoint, g_column)
    SELECT
        scope_name,
        scope_value,
        rat,
        cgis_list,
        ST_Union(g_mpoint),
        ST_ConvexHull(ST_Union(g_column))
    FROM locations_gis_staging
    WHERE qtype = 'deduce_polygon'
    GROUP BY scope_name, scope_value, rat, cgis_list
"""

SQL_GEOJSON = """
    INSERT INTO locations_gis_tmp (scope_name, scope_value, rat, cgis_list, g_mpoint, g_column)
    SELECT
        scope_name,
        scope_value,
        rat,
        cgis_list,
        ST_Union(g_mpoint),
        g_column
    FROM locations_gis_staging
    WHERE qtype = 'geojson'
    GROUP BY scope_name, scope_value, rat, cgis_list, g_column
"""


class LocationsGisReader(object):

    def __init__(self, chunk_sz=CHUNK_SIZE, srid=4326):
        self.logger = ScriptLogger.getLogger()
        self.chunk_sz = chunk_sz
        self.srid = srid

    def get_tmp_gis_template(self):
        template = "(%(scope_name)s, %(scope_value)s, %(rat)s, %(cgis_list)s, %(qtype)s, ST_Force2D(ST_GeomFromText(%(mpoint)s, {srid})), ST_Force2D(ST_GeomFromText(%(mpoint)s, {srid})))".format(srid=self.srid)
        return template

    def get_tmp_geojson_template(self):
        template = "(%(scope_name)s, %(scope_value)s, %(rat)s, %(cgis_list)s, %(qtype)s, ST_Force2D(ST_GeomFromText(%(mpoint)s, {srid})), ST_Force2D(ST_GeomFromGeoJSON(%(geojson)s)))".format(srid=self.srid)
        return template

    def get_tmp_simplification_geojson_template(self):
        template = "(%(scope_name)s, %(scope_value)s, %(rat)s, %(cgis_list)s, %(qtype)s, ST_Force2D(ST_GeomFromText(%(mpoint)s, {srid})), ST_SimplifyPreserveTopology(ST_Force2D(ST_GeomFromGeoJSON(%(geojson)s)), %(simplification_level)s))".format(srid=self.srid)
        return template

    def _get_decoded_geojson(self, row):
        if isinstance(row["geojson"], dict):
            decoded_geojson = row["geojson"]
        elif isinstance(row["geojson"], basestring):
            decoded_geojson = simplejson.loads(row["geojson"])
        else:
            raise Exception("Unknown type of geojson - not a string nor a dict")

        return decoded_geojson

    def _get_valid_postgis_geojson(self, row):

        """ https://tools.ietf.org/html/rfc7946
            A GeoJSON object represents a Geometry, Feature, or collection of Features.
            Geometry object's type: Point, MultiPoint, LineString, MultiLineString,
            Polygon, MultiPolygon, GeometryCollection
            FeatureCollection Object: A FeatureCollection object has a member
            with the name "features".  The value of "features" is a JSON array
            A Feature object has a member with the name "geometry"
        """

        decoded_geojson = self._get_decoded_geojson(row)

        # {"type": "GeometryCollection", "geometries": ....}
        # or {"type": ..., "coordinates": ....}
        # where type one of: Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon
        geojson = simplejson.dumps(decoded_geojson)

        if "geometry" in decoded_geojson:
            # {"geometry": {"type": ..., "coordinates": ...}}
            geojson = simplejson.dumps(decoded_geojson["geometry"])

        elif "type" in decoded_geojson:
            if decoded_geojson["type"] == "FeatureCollection":
                # convert to GeometryCollection
                valid_postgis_geojson = {
                    "type": "GeometryCollection",
                    "geometries": []
                }

                features = decoded_geojson["features"]

                assert isinstance(features, list)

                for f in features:
                    if "geometry" in f:
                        valid_postgis_geojson["geometries"].append(f["geometry"])

                geojson = simplejson.dumps(valid_postgis_geojson)

        return geojson

    def read_chunk_from_gis_file_for_staging(self, gis_file):
        logger = ScriptLogger.getLogger()
        t0 = time.time()
        # In mapping frameworks spatial coordinates are often in order of latitude and longitude.
        # In spatial databases spatial coordinates are in x = longitude, and y = latitude.
        with open(gis_file, 'r') as fp:
            data = simplejson.load(fp)

        logger.perf('gis_loader.reader', "Data loaded from file: %s" % gis_file, t0)

        chunks_dict = {
            "geojson": {"template": self.get_tmp_geojson_template(), "chunk": [], "idx": 0},
            "geojson.simplification": {"template": self.get_tmp_simplification_geojson_template(), "chunk": [], "idx": 0},
            "cgi": {"template": self.get_tmp_gis_template(), "chunk": [], "idx": 0},
            "deduce_centroid": {"template": self.get_tmp_gis_template(), "chunk": [], "idx": 0},
            "deduce_polygon": {"template": self.get_tmp_gis_template(), "chunk": [], "idx": 0}
        }

        for idx, row in enumerate(data):

            if not row.get("cgis_list"):
                row["cgis_list"] = []

            if not row.get("mpoint"):
                row["mpoint"] = None

            if not row.get("rat"):
                row["rat"] = ""

            if row.get("geojson"):

                try:
                    row["geojson"] = self._get_valid_postgis_geojson(row)
                except Exception as ex:
                    self.logger.error("gis_loader.valid_postgis_geojson", "Error: %s - Skip row: %s" % (ex, row))
                    continue

                row["qtype"] = "geojson"
                if row.get("simplification_level"):
                    chunks_dict["geojson.simplification"]["chunk"].append(row)
                    chunks_dict["geojson.simplification"]["idx"] += 1
                else:
                    chunks_dict["geojson"]["chunk"].append(row)
                    chunks_dict["geojson"]["idx"] += 1
            else:
                if row.get("geojson.deduce_centroid"):
                    row["qtype"] = "deduce_centroid"
                    chunks_dict["deduce_centroid"]["chunk"].append(row)
                    chunks_dict["deduce_centroid"]["idx"] += 1
                elif row.get("geojson.deduce_polygon"):
                    row["qtype"] = "deduce_polygon"
                    chunks_dict["deduce_polygon"]["chunk"].append(row)
                    chunks_dict["deduce_polygon"]["idx"] += 1
                elif row.get("geom"):
                    row["qtype"] = "cgi"
                    chunks_dict["cgi"]["chunk"].append(row)
                    chunks_dict["cgi"]["idx"] += 1
                else:
                    # default action: deduce a centroid from collected cgis
                    row["qtype"] = "deduce_centroid"
                    chunks_dict["deduce_centroid"]["chunk"].append(row)
                    chunks_dict["deduce_centroid"]["idx"] += 1

            for item in chunks_dict.itervalues():
                chunk_idx = item["idx"]
                if chunk_idx % self.chunk_sz == 0 and chunk_idx > 0:
                    yield item["template"], item["chunk"]
                    del item["chunk"][:]

        for item in chunks_dict.itervalues():
            if len(item["chunk"]) > 0:
                yield item["template"], item["chunk"]


class LocationsGisLoader(object):

    def __init__(self, cur):
        self.logger = ScriptLogger.getLogger()
        self.cur = cur

    def initialise_db_relations(self, srid=4326):
        # intialise db tables
        self.cur.execute(DUPLICATE_PROD_TABLE)
        self.logger.debug('gis_loader.init', "Duplicate production locations_gis table [%s]" % self.cur.mogrify(DUPLICATE_PROD_TABLE), syslog=False)
        self.cur.execute(STAGING_TMP_TABLE.format(srid=srid))
        self.logger.debug('gis_loader.init', "Staging table [%s]" % self.cur.mogrify(STAGING_TMP_TABLE.format(srid=srid)), syslog=False)

        return

    def load_staging_locations_gis(self, gis_file, chunk_sz, srid=4326):
        t0 = time.time()
        rowcount = 0

        locations_reader = LocationsGisReader(chunk_sz, srid=4326)

        locations_gis_columns = ["scope_name", "scope_value", "rat", "cgis_list", "qtype", "g_mpoint", "g_column"]
        EX_VALUES_COMMAND_TEMPLATE = """
            INSERT INTO {0} ({1}) VALUES %s
        """.format("locations_gis_staging", ",".join(locations_gis_columns))
        counter = 0
        for template, lines in locations_reader.read_chunk_from_gis_file_for_staging(gis_file):
            psycopg2.extras.execute_values(self.cur, EX_VALUES_COMMAND_TEMPLATE, lines, template=template, page_size=chunk_sz)
            self.logger.perf('gis_loader.staging', "Executing for template %s - counter=%d - current_rows=%d" % (str(template), counter, self.cur.rowcount), t0)
            t0 = time.time()
            rowcount += self.cur.rowcount
            counter += 1

            self.logger.debug("gis_loader.staging", "locations_gis_staging table populated total_rows=%s" % rowcount)

        return rowcount

    def load_data_to_tmp_gis_table(self):
        self.cur.execute(SQL_CGI)
        self.logger.debug('gis_loader.cgi', "SQL_CGI [%s] - rows: %s" % (self.cur.mogrify(SQL_CGI), self.cur.rowcount), syslog=False)
        self.cur.execute(SQL_DEDUCED_CENTROID)
        self.logger.debug('gis_loader.centroid', "SQL_DEDUCED_CENTROID [%s] - rows: %s" % (self.cur.mogrify(SQL_DEDUCED_CENTROID), self.cur.rowcount), syslog=False)
        self.cur.execute(SQL_DEDUCED_POLYGON)
        self.logger.debug('gis_loader.polygon', "SQL_DEDUCED_POLYGON [%s] - rows: %s" % (self.cur.mogrify(SQL_DEDUCED_POLYGON), self.cur.rowcount), syslog=False)
        self.cur.execute(SQL_GEOJSON)
        self.logger.debug('gis_loader.geojson', "SQL_GEOJSON [%s] - rows: %s" % (self.cur.mogrify(SQL_GEOJSON), self.cur.rowcount), syslog=False)

        return

    def load_data_to_production_gis_table(self):
        self.cur.execute(DROP_TABLE)
        self.logger.debug('gis_loader.prod', "Drop gis table [%s]" % self.cur.mogrify(DROP_TABLE), syslog=False)

        self.cur.execute(RENAME_TMP_TABLE)
        self.logger.debug('gis_loader.prod', "Rename tmp gis table [%s]" % self.cur.mogrify(RENAME_TMP_TABLE), syslog=False)

        return


def main():
    parser = ArgumentParser(description='Tool for loading gis data to locations_gis table.')
    parser.add_argument("--fname", default=None, dest="fname", help="""gis locations file to load to db.""")
    parser.add_argument("--chunk-size", type=int, default=CHUNK_SIZE, dest="chunk_size", help="""Overrides chunk size for db insertion.""")
    parser.add_argument("--srid", type=int, default=4326, dest="srid", help="""Overrides default spatial reference id (4326).""")
    options = parser.parse_args()

    logger = ScriptLogger.getLogger(component="analytics.gis_loader",
                                    console=True,
                                    syslog=False,
                                    level='DEBUG')

    if not options.fname:
        parser.error("Abort execution - Filename not given.")

    if not os.path.exists(options.fname):
        parser.error("Abort execution - Filename=%s not exists." % options.fname)

    t0 = time.time()
    try:
        chunk_sz = int(options.chunk_size)
        srid = int(options.srid)
        logger.debug("gis_loader.opts", "Chunk size: %d - SRID=%s given" % (chunk_sz, srid))
        db_conf = get_pg_conf()
        pg_helper = PgHelper(db_conf)
        with pg_helper.with_sql_connection() as db_connection:
            cur = db_connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

            # initialise db object
            db_loader = LocationsGisLoader(cur)

            # intialise db tables and relations
            db_loader.initialise_db_relations(srid=srid)

            # load data to staging table
            total_rows = db_loader.load_staging_locations_gis(options.fname, chunk_sz=chunk_sz, srid=srid)
            logger.perf('gis_loader.staging', "locations_gis_staging table populated total_rows=%s" % total_rows, t0)

            # load data to production tables
            db_loader.load_data_to_tmp_gis_table()

            # rename tmp table to production gis table
            db_loader.load_data_to_production_gis_table()

            db_connection.commit()
    except Exception as ex:
        logger.error("gis_loader.exc", "Error: %s while inserting data in locations_gis table" % ex)
        sys.exit(1)

    logger.perf('gis_loader', "Done!", t0)

    return 0


if __name__ == "__main__":
    sys.exit(main())

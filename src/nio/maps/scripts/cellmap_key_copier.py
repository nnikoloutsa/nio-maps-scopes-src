import json

COPIED_CELLMAP_KEYS = ["region", "province", "city", "district", "village", "geoloc_simple", "location", "cellname"]

with open("cellmap.json", "r") as fp:
    cellmap = json.load(fp)

location_counts = {}

new_cellmap = []

for e in cellmap:

    ne = {}

    for s in COPIED_CELLMAP_KEYS:

        cur_value = e.get(s, None)

        if cur_value is None:
            continue

        ne[s] = cur_value

    new_cellmap.append(ne)

with open("cellmap.new.json", "w") as fp:
    json.dump(new_cellmap, fp, indent=4)

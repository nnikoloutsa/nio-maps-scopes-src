import json

with open("scopes.json", "r") as fp:
    scopes = json.load(fp)["scopes"]

with open("cellmap.json", "r") as fp:
    cellmap = json.load(fp)

location_counts = {}

for e in cellmap:
    for s in scopes:
        key = s["cellmap_key"]

        cur_location = e.get(key, None)

        if cur_location is None:
            continue

        if e.get("village", None) and not e.get("city"):
            print e

        if key not in location_counts:
            location_counts[key] = set()

        cur_counts = location_counts[key]

        cur_counts.add(cur_location)


for k, v in location_counts.items():
    print k, len(v)

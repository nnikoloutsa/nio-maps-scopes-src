# NIO_COMPONENT analytics.maps
import sys
import json


def main(customer):

    SCOPE_PATH = "customers/%s/config/scopes.json" % customer
    SCOPE_ID_NAME_MAP = "customers/%s/config/scope_id_name.json" % customer
    SCOPEMAP_PATH = "customers/%s/config/scopemap.json" % customer

    with open(SCOPEMAP_PATH, "r") as fp:
        scopemap = json.load(fp)

    with open(SCOPE_PATH, "r") as fp:
        scopes = json.load(fp)

    with open(SCOPE_ID_NAME_MAP, "r") as fp:
        scope_id_name_map = json.load(fp)

    seen = set()
    for s in scopes:

        if s["key"] in seen:
            continue
        seen.add(s["key"])

        print "Examining %s" % s["key"]

        legacy_map = {k["name"]: k["id"] for k in scopemap[s["key"]]}
        id_name_map = {k["name"]: k["id"] for k in scope_id_name_map[s["key"]]}

        for name in legacy_map:
            if name not in id_name_map:
                print "\tLegacy %s not in id_name_map" % name
            elif legacy_map[name] != id_name_map[name]:
                print "\tLegacy and id_name_map have different id for %s: %s vs %s" % (name, legacy_map[name], id_name_map[name])

        for name in id_name_map:
            if name not in legacy_map:
                print "\New %s not in legacy map" % name


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "Usage: python compare_fixed_id_name_to_legacy_scopemap.py <customer>"
        sys.exit(-1)

    customer = sys.argv[1].strip()

    if ".." in customer:
        # todo check customer directories
        print "Invalid customer name"
        sys.exit(-1)

    sys.exit(main(customer))

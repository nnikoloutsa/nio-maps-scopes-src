# NIO_COMPONENT analytics.maps
import sys
import json


def main(customer):
    with open("customers/%s/config/scopemap.json" % customer, "r") as fp:
        scopemap = json.load(fp)[0]

    non_scope_items = {
      "dns": scopemap["dns"],
      "ip_group": scopemap["ip_group"],
    }

    with open("customers/%s/artifacts/dns_ip_dimensions.json" % customer, "w") as fp:
        json.dump(non_scope_items, fp, indent=4)


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "Usage: python extract_non_scope_items_from_scopemap.py <customer>"
        sys.exit(-1)

    customer = sys.argv[1].strip()

    if ".." in customer:
        # todo check customer directories
        print "Invalid customer name"
        sys.exit(-1)

    sys.exit(main(customer))

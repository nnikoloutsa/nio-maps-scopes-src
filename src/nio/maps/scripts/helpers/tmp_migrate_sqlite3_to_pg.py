# NIO_COMPONENT analytics.maps
import psycopg2
import psycopg2.extras
import sys
import sqlite3
import json


class PGHelper:
    @staticmethod
    def _get_database():
        conn = psycopg2.connect(
            "dbname='%s' user='%s' password='%s' host='localhost'" % ("analytics", "nio", "analytics")
        )
        return conn

    @staticmethod
    def get_connection_and_cursor():
        conn = PGHelper._get_database()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        return conn, cur

    @staticmethod
    def sql_query(querySQL, values):
        conn = PGHelper._get_database()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(querySQL, values)
        rows = [dict(r) for r in cur]
        cur.close()
        conn.close()
        return rows

    @staticmethod
    def sql_execute(querySQL, values):
        conn = PGHelper._get_database()
        cur = conn.cursor()
        cur.execute(querySQL, values)
        conn.commit()
        cur.close()
        conn.close()


def main(legacy_db_path):

    # prepare db tables
    query_SQL = """
        DROP TABLE IF EXISTS maps_locations;

        CREATE TABLE maps_locations (
            uid SERIAL PRIMARY KEY NOT NULL,
            id INTEGER NOT NULL,
            name CHARACTER VARYING(255) NOT NULL,
            scope  CHARACTER VARYING(255) NOT NULL,
            parent_id INTEGER NOT NULL,
            parent_scope CHARACTER VARYING(255) NOT NULL,
            info TEXT,
            coordinates JSONB,
            geojson JSONB,
            hardware TEXT,
            lft INTEGER,
            rgt INTEGER
        );

        CREATE INDEX maps_id_idx ON maps_locations(id);

        CREATE INDEX maps_scope_id_idx ON maps_locations(scope, id);

        CREATE INDEX maps_scope_name_idx ON maps_locations(scope, name);
    """

    PGHelper.sql_execute(query_SQL, [])

    sq_conn = sqlite3.connect(legacy_db_path)
    sq_cur = sq_conn.cursor()
    locations = sq_cur.execute("SELECT uid, id, name, scope, parent_id, parent_scope, info, coordinates, geojson, hardware, left, right FROM locations", [])

    insert_SQL = """
        INSERT INTO maps_locations (
            id,
            name,
            scope,
            parent_id,
            parent_scope,
            info,
            coordinates,
            geojson,
            hardware,
            lft,
            rgt
        ) VALUES (
            %(id)s,
            %(name)s,
            %(scope)s,
            %(parent_id)s,
            %(parent_scope)s,
            %(info)s,
            %(coordinates)s,
            %(geojson)s,
            %(hardware)s,
            %(lft)s,
            %(rgt)s
        )
    """
    pg_conn, pg_cur = PGHelper.get_connection_and_cursor()
    pg_cur.execute("BEGIN TRANSACTION", [])

    locations = list(locations)

    total = len(locations)

    for i, l in enumerate(locations):

        geojson = l[8]
        try:
            if geojson not in [None, ""]:
                geojson = json.loads(geojson)
        except Exception as e:
            print e, geojson
            pass

        values = {
            "id": l[1],
            "name": l[2],
            "scope": l[3],
            "parent_id": l[4],
            "parent_scope": l[5],
            "info": l[6] or None,
            "coordinates": l[7] or None,
            "geojson": json.dumps(geojson) if geojson else None,
            "hardware": l[9],
            "lft": l[10],
            "rgt": l[11],
        }
        pg_cur.execute(insert_SQL, values)

        if i % (total / 10) == 0:
            print "%s entries added" % i

    pg_conn.commit()


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "Usage: python tmp_migrate_sqlite3_to_pg.py <location_db_path>"
        sys.exit(-1)

    legacy_db_path = sys.argv[1].strip()

    sys.exit(main(legacy_db_path))
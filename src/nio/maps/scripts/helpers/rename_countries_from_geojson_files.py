# NIO_COMPONENT analytics.maps
import os
import json

COUNTRIES_GEOJSON_DIR = "global/geojson/country"

for filename in os.listdir(COUNTRIES_GEOJSON_DIR):
    if not filename.endswith(".json"):
        continue

    if filename[3] == ".":
        print "already renamed skipping"
        continue

    with open(COUNTRIES_GEOJSON_DIR + "/" + filename, "r") as fp:
        geojson = json.load(fp)

    iso_a3 = geojson.get("properties", {}).get("ISO_A3", None)
    if iso_a3 is None:
        print "No ISO_A3 found", filename
        continue

    if iso_a3 == "-99":
        print "Obscure ISO_A3 -99 found", filename
        continue

    print "renaming", filename, "to", iso_a3 + "." + filename
    os.rename(COUNTRIES_GEOJSON_DIR + "/" + filename, COUNTRIES_GEOJSON_DIR + "/" + iso_a3 + "." + filename)
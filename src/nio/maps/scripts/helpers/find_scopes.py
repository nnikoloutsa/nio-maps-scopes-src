#!/usr/bin/env python
# NIO_COMPONENT analytics.maps
import os
import sys
import json


def main():

    CUSTOMERS = {f for f in os.listdir("./data/customers") if not f.startswith(".")}

    for c in CUSTOMERS:
        scopes_file = "./data/customers/%s/config/scopes.json" % c
        copy_file = "./data/customers/%s/config/copy.txt" % c
        print "Customer: %s" % c
        if os.path.exists(scopes_file):
            if os.path.exists(copy_file):
                print "copy.txt"
                continue
            with open(scopes_file, "r") as fp:
                data = json.load(fp)
                for scope in data["scopes"]:
                    print "\t", "scope_name:", scope["scope_name"], "analytics_key:", scope["analytics_key"], "scopemap_keys:", scope["scopemap_keys"]


if __name__ == "__main__":
    sys.exit(main())

# NIO_COMPONENT analytics.maps
import sys
import json
import os
from collections import defaultdict


def calculate_location_hierarchies(scopes, cellmap):
    # find all keys we want, basically the set of unique
    # scope_name from scopes.json
    scope_map = {}
    unique_scope_names = {}
    for si in scopes:
        if not si["legacy"]:
            scope_map[si["scope_name"]] = si
            unique_scope_names[si["scope_name"]] = set()

    # now we need to find the hierarchy
    # we can do by ordering by number of items
    for cell in cellmap:
        for s in unique_scope_names:
            cellmap_key = scope_map[s]["cellmap_key"]
            if cellmap_key not in cell:
                continue
            location_name = cell[cellmap_key]
            unique_scope_names[s].add(location_name)

    full_hierarchy = []

    for s, locations in unique_scope_names.items():
        full_hierarchy.append((s, len(locations)))

    full_hierarchy = [s[0] for s in sorted(full_hierarchy, key=lambda x: x[1])]

    scope_hierarchy = []
    for scope_name in full_hierarchy:
        scope_hierarchy.append(scope_name)

    return scope_hierarchy


def main(customer):

    CELLMAP_PATH = "customers/%s/config/cellmap.json" % customer
    SCOPES_PATH = "customers/%s/config/scopes.json" % customer
    ID_NAME_LOCATION_PATH = "customers/%s/config/scope_id_name.json" % customer

    if not os.path.exists(CELLMAP_PATH):
        print "cellmap.json is required for extracting id/name info for locations"
        sys.exit(-1)

    if not os.path.exists(SCOPES_PATH):
        print "scopes.json is required for extracting id/name info for locations"
        sys.exit(-1)

    with open(SCOPES_PATH, "r") as fp:
        scopes = json.load(fp)

    with open(CELLMAP_PATH, "r") as fp:
        cellmap = json.load(fp)

    scope_hierarchy = calculate_location_hierarchies(scopes, cellmap)

    print "Scope hierarchy that will be examined:", scope_hierarchy

    locations = {}

    examined_hierarchy = scope_hierarchy[:] + ["cell"]

    for cell in cellmap:
        for i, scope_name in enumerate(examined_hierarchy):

            if scope_name == "cell":
                name = cell["location"]
            elif scope_name == "bts":
                name = cell["siteid"]
            else:
                name = cell[scope_name]

            if i == 0:
                parent_scope_name = "nation"
                parent_name = "Indonesia"
            else:
                parent_scope_name = examined_hierarchy[i - 1]
                if parent_scope_name == "bts":
                    parent_name = cell["siteid"]
                else:
                    parent_name = cell[parent_scope_name]

            key = (parent_scope_name, scope_name, name)

            if key not in locations:
                locations[key] = set()

            locations[key].add(parent_name)

    for (parent_scope_name, scope_name, name), locations_set in locations.items():
        if len(locations_set) > 1:
            print "-----------------------"
            print "%s: %s appears in: %s %s" % (scope_name, name, ", ".join(locations_set), parent_scope_name)


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "Usage: python find_non_unique_locations.py <customer>"
        sys.exit(-1)

    customer = sys.argv[1].strip()

    if ".." in customer:
        # todo check customer directories
        print "Invalid customer name"
        sys.exit(-1)

    sys.exit(main(customer))

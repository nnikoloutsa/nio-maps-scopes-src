# NIO_COMPONENT analytics.maps
import sys
import json
import os
from collections import defaultdict


def main(customer):

    CELLMAP_PATH = "customers/%s/config/cellmap.json" % customer
    SCOPES_PATH = "customers/%s/config/scopes.json" % customer
    ID_NAME_LOCATION_PATH = "customers/%s/config/scope_id_name.json" % customer

    if not os.path.exists(CELLMAP_PATH):
        print "cellmap.json is required for extracting id/name info for locations"
        sys.exit(-1)

    if not os.path.exists(SCOPES_PATH):
        print "scopes.json is required for extracting id/name info for locations"
        sys.exit(-1)

    if os.path.exists(ID_NAME_LOCATION_PATH):
        print "scope_id_name.json already exists! Attempt to change immutable file!"
        sys.exit(-1)

    with open(SCOPES_PATH, "r") as fp:
        scopes = json.load(fp)

    with open(CELLMAP_PATH, "r") as fp:
        cellmap = json.load(fp)

    # give permanent id to locations
    # by using the order by which they were seen in
    # cellmap json
    # this should only be run once
    # and the file should be manually updated subsequently
    scope_id_name_map = defaultdict(lambda: [])
    seen = set()
    for cell in cellmap:
        for s in scopes:
            scope_id_name = scope_id_name_map[s["scope_name"]]
            if s["cellmap_key"] in cell:
                name = cell[s["cellmap_key"]]
                if (s["scope_name"], name) in seen:
                    continue
                seen.add((s["scope_name"], name))
                scope_id_name.append({"name": name, "id": len(scope_id_name) + 1})

    with open(ID_NAME_LOCATION_PATH, "w") as fp:
        json.dump(scope_id_name_map, fp, indent=4)


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "Usage: python create_permanent_id_name_map.py <customer>"
        sys.exit(-1)

    customer = sys.argv[1].strip()

    if ".." in customer:
        # todo check customer directories
        print "Invalid customer name"
        sys.exit(-1)

    sys.exit(main(customer))

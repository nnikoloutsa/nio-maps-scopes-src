# NIO_COMPONENT analytics.maps
import sys
import sqlite3
import json
import os

from ...lib import geo_helper
from ...lib import utils_converter


def sqlite_to_directory(customer_dir, scope, sqlite_file, table):

    conn = sqlite3.connect(sqlite_file)
    cur = conn.cursor()

    scopes = []
    if scope:
        scopes = [scope]
    else:
        scopes = [row[0] for row in list(cur.execute("SELECT DISTINCT(scope) FROM %s" % table)) if row[0] != "cell"]

    hierarchy = list(cur.execute("SELECT DISTINCT scope, parent_scope FROM %s" % table))
    print hierarchy


    for scope in scopes:
        scope_dir = "%s/%s" % (customer_dir, scope)
        if os.path.exists(scope_dir):
            print "%s already exists! Cannot replace existing files!" % scope_dir
            sys.exit(-1)

        os.makedirs(scope_dir)

    scopes_array = "(%s)" % ",".join(["?" for r in scopes])

    sql_query = """
        SELECT id, name, info, scope, geojson
        FROM
            %(table)s
        WHERE
            scope in %(scopes_array)s
    """ % {
        "table": table,
        "scopes_array": scopes_array,
    }

    locations = list(cur.execute(sql_query, scopes))

    conn.close()
    scope_stats = {}

    for l in locations:
        lid, name, info, scope, geojson = l[0], l[1], l[2], l[3], l[4]

        if scope not in scope_stats:
            scope_stats[scope] = {
                "info": 0,
                "geojson": {
                    "count": 0,
                    "types": {},
                },
                "empty": 0
            }

        json_content = {}
        try:
            if info:
                info = json.loads(info)
                json_content = geo_helper.Coord(lat=info["lat"], lon=info["lon"]).geojson()
                scope_stats[scope]["info"] += 1
            elif geojson:
                json_content = json.loads(geojson)
                scope_stats[scope]["geojson"]["count"] += 1
                # "properties": {
                #     "cluster": "EB-BNA-BADUNG",
                #     "region": "EAST & BALI NUSRA",
                #     "area": "BALI NUSRA",
                #     "micro": "MC-BADUNG_2"
                # }

                if "properties" in json_content:
                    del json_content["properties"]

                if json_content["type"] not in scope_stats[scope]["geojson"]["types"]:
                    scope_stats[scope]["geojson"]["types"][json_content["type"]] = 0

                scope_stats[scope]["geojson"]["types"][json_content["type"]] += 1

            else:
                name += ".empty"
                scope_stats[scope]["empty"] += 1
                print 'Scope %s Record without info and geojson:' % (scope, ), l
        except Exception, e:
            print str(e)
            print "----> Empty coords for %s (%s)" % (name, lid)
            continue

        scope_name_path = utils_converter.scope_name_to_path(name)
        json_path = "%s/%s/%s.json" % (customer_dir, scope, scope_name_path)
        with open(json_path, "w") as ofp:
            json.dump(json_content, ofp, indent=2)

    print json.dumps(scope_stats, indent=4)

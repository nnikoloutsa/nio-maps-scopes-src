#!/usr/bin/env python
# NIO_COMPONENT analytics.maps
import psycopg2
import psycopg2.extras
import sys
import json

from argparse import ArgumentParser

from niolib.readers.credentials_reader import CredentialsReader


# duplicates helper class so that locations_loader can run standalone -- fix
class ProgessPercentage(object):
    def __init__(self, message, total):
        self.message = message
        self.total = total
        self.prev_percentage_done = None

    def print_progress(self, i):
        if i == self.total:
            raise Exception("More items than total count reported for progress display")
        percentage_done = ((i + 1) * 100) / self.total
        if percentage_done != self.prev_percentage_done:
            item = 0 if percentage_done == 0 else i + 1
            print self.message + " - Done: %(percentage_done)s%% (#%(item)s)" % {"percentage_done": percentage_done, "item": item}
            self.prev_percentage_done = percentage_done
# end


class PGHelper:
    @staticmethod
    def _get_database():

        cred_filename = "/etc/opt/nio/credentials.json"
        cred_keyfile = "/etc/opt/nio/credentials.key"
        cred = CredentialsReader(filename=cred_filename, keyfile=cred_keyfile)
        db_cred = cred.get_all().get("pg_analytics")
        db_name = db_cred.get("database")
        db_user = db_cred.get("username")
        db_pass = db_cred.get("password")

        conn = psycopg2.connect(
            "dbname='%s' user='%s' password='%s' host='localhost'" % (db_name, db_user, db_pass)
        )
        return conn

    @staticmethod
    def get_connection_and_cursor():
        conn = PGHelper._get_database()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        return conn, cur

    @staticmethod
    def sql_query(querySQL, values):
        conn = PGHelper._get_database()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(querySQL, values)
        rows = [dict(r) for r in cur]
        cur.close()
        conn.close()
        return rows

    @staticmethod
    def sql_execute(querySQL, values):
        conn = PGHelper._get_database()
        cur = conn.cursor()
        cur.execute(querySQL, values)
        conn.commit()
        cur.close()
        conn.close()


def main():

    parser = ArgumentParser()
    parser.add_argument("--locations-file", type=str, required=True, help="Path to locations file to load")

    args = parser.parse_args()

    # prepare db tables
    query_SQL = """
        DELETE FROM maps_meta;
        DELETE FROM maps_pois;
        DELETE FROM maps_pois_cgi;
        DELETE FROM maps_locations;
        DELETE FROM maps_hierarchies;
    """

    PGHelper.sql_execute(query_SQL, [])

    insert_SQL = """
        INSERT INTO maps_locations (
            hierarchy,
            id,
            name,
            scope,
            parent_id,
            parent_scope,
            coordinates,
            geojson,
            mcc,
            lon,
            lat,
            lft,
            rgt
        ) VALUES (
            %(hierarchy)s,
            %(id)s,
            %(name)s,
            %(scope)s,
            %(parent_id)s,
            %(parent_scope)s,
            %(coordinates)s,
            %(geojson)s,
            %(mcc)s,
            %(lon)s,
            %(lat)s,
            %(lft)s,
            %(rgt)s
        )
    """
    pg_conn, pg_cur = PGHelper.get_connection_and_cursor()
    pg_cur.execute("BEGIN TRANSACTION", [])

    with open(args.locations_file, "r") as fp:
        data = json.load(fp)

    for hierarchy, locations in data["hierarchy_locations_map"].items():

        pp = ProgessPercentage("Hierarchy %s" % hierarchy, len(locations))

        for i, l in enumerate(locations):

            geojson = l.get("geojson", None)
            try:
                if geojson not in [None, ""]:
                    geojson = json.loads(geojson)
            except Exception as e:
                print e, geojson

            coordinates = l.get("coordinates", None) or None
            try:
                if coordinates:
                    coordinates = json.loads(coordinates)
            except Exception as e:
                print e, coordinates

            values = {
                "hierarchy": hierarchy,
                "id": l["id"],
                "name": l["name"],
                "scope": l["scope"],
                "parent_id": l["parent_id"],
                "parent_scope": l["parent_scope"],
                "coordinates": json.dumps(coordinates) if coordinates else None,
                "geojson": json.dumps(geojson) if geojson else None,
                "mcc": l.get("mcc", None),
                "lon": l.get("lon", None),
                "lat": l.get("lat", None),
                "lft": l.get("left"),
                "rgt": l.get("right"),
            }
            pg_cur.execute(insert_SQL, values)

            pp.print_progress(i)

    # add pois
    insert_SQL_pois = """
        INSERT INTO maps_pois (
            id,
            name,
            info,
            poi_group,
            coordinates,
            geojson,
            lon,
            lat
        ) VALUES (
            %(id)s,
            %(name)s,
            %(info)s,
            %(poi_group)s,
            %(coordinates)s,
            %(geojson)s,
            %(lon)s,
            %(lat)s
        )
    """

    insert_SQL_pois_cgi = """
        INSERT INTO maps_pois_cgi (
            poi_id,
            cgi
        ) VALUES (
            %(poi_id)s,
            %(cgi)s
        )
    """

    pp = ProgessPercentage("Adding POIs", len(data["pois"]))

    for i, p in enumerate(data["pois"]):

        geojson = p.get("geojson", None)
        try:
            if geojson not in [None, ""]:
                # FIXME here we load just to dump again below
                # optimize this away if we don't need to read inside
                geojson = json.loads(geojson)
        except Exception as e:
            print e, geojson

        values = {
            "id": p["id"],
            "name": p["name"],
            "info": "",
            "poi_group": "",
            "geojson": json.dumps(geojson) if geojson else None,
            "coordinates": json.dumps({}),
            # XXX setting this to none for now
            # we could calculate their centroid
            "lon": None,
            "lat": None,
        }
        pg_cur.execute(insert_SQL_pois, values)

        for location in p["locations"]:
            values = {
                "poi_id": p["id"],
                "cgi": location
            }
            pg_cur.execute(insert_SQL_pois_cgi, values)

        pp.print_progress(i)

    pg_conn.commit()

    # add meta
    insert_meta_SQL = """
        INSERT INTO maps_meta (
            key,
            info
        ) VALUES (
            %(key)s,
            %(info)s
        )
    """

    values = {
        "key": "meta",
        "info": json.dumps(data.get("meta", {})),
    }

    pg_cur.execute(insert_meta_SQL, values)
    pg_conn.commit()

    print "meta added"
    # add hierarchy info

    insert_hierarchies_SQL = """
        INSERT INTO maps_hierarchies (
            hierarchy,
            info
        ) VALUES (
            %(hierarchy)s,
            %(info)s
        )
    """

    hierarchies = data.get("hierarchies", {})
    for hierarchy, info in hierarchies.items():
        values = {
            "hierarchy": hierarchy,
            "info": json.dumps(info)
        }
        pg_cur.execute(insert_hierarchies_SQL, values)
        print "hierarchy: %s, info added" % hierarchy

    pg_conn.commit()


if __name__ == "__main__":
    sys.exit(main())

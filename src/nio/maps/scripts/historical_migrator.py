#!/usr/bin/env python
# NIO_COMPONENT analytics.maps
import os
import sys
import json

from argparse import ArgumentParser

def main():

    parser = ArgumentParser()
    parser.add_argument("--migration-file", type=str, required=True, help="Path to locations file to load")

    args = parser.parse_args()

    with open(args.migration_file, "r") as fp:
        data = json.load(fp)

    count = 0
    min_ts = 1000000000000000
    max_ts = 0
    for root, dirs, files in os.walk("/var/opt/nio/historical/"):
        for filename in files:
            filename_full = root + os.sep + filename
            changed = False
            splitted = filename.split(".")
            for operation in data["historical"]["operations"]:
                key, value = operation["dimension"]
                if key in splitted and not changed:
                    count += 1
                    ts = filename_full.split(".")[-1]
                    if ts.isdigit() and ts != "1":
                        ts = int(ts)
                        if ts > max_ts:
                            max_ts = ts
                        if ts < min_ts:
                            min_ts = ts
                    filename_changed = filename_full.replace(key, value)
                    print "%s -> %s" % (filename_full, filename_changed)
                    changed = True
    
    print "count: %s" % count
    print "min_ts: %s" % min_ts
    print "max_ts: %s" % max_ts
#os.rename(root + os.sep + filename, root + os.sep + filename_changed)


if __name__ == "__main__":
    sys.exit(main())

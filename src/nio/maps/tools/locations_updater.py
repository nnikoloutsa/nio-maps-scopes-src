#!/usr/bin/env python
# NIO_COMPONENT analytics.maps
import json
import os
import shutil
import sys

from ..lib import utils, scopemap_merger
from ..lib.utils_reader import ConfigReader
from ..lib.gis_helper import get_locations_gis


def _update_locations(customer, config_reader, save_optimization=False):
    print "Updating locations..."

    hierarchies = {}
    hierarchy_locations_map = {
        "global": utils.get_global_locations(config_reader.countries),
    }

    hierarchy_cardinalities_map = {
        "global": len(hierarchy_locations_map.get("global")),
    }

    hierarchy_names = config_reader.scopes_info["hierarchies"].keys()

    for hierarchy in hierarchy_names:
        locations, locations_per_scope_count = utils.get_locations_for_hierarchy(hierarchy, config_reader, save_optimization)
        hierarchy_locations_map[hierarchy] = locations
        hierarchy_cardinalities_map[hierarchy] = locations_per_scope_count

    hierarchies = utils.get_hierarchies(config_reader.scopes_info, hierarchy_names)

    ic_scopes_map = get_ic_scopes_map(hierarchies, hierarchy_cardinalities_map)
    if not ic_scopes_map:
        print "interconnect_cs_location disabled!"

    locations_db_data = {
        "hierarchy_locations_map": hierarchy_locations_map,
        "hierarchies": hierarchies,
        "meta": config_reader.scopes_info.get("meta", {}),
        "pois": config_reader.pois_info,
    }

    print "Done!"

    return locations_db_data, ic_scopes_map


def get_ic_scopes_map(hierarchies, hierarchy_cardinalities_map):
    """ Determines the N1, N2 scopes for the interconnect cs functionality """
    MAIN_HIERARCHY = "mapview"
    IC_CS_MAX_CARDINALITY = 500

    if MAIN_HIERARCHY not in hierarchies or MAIN_HIERARCHY not in hierarchy_cardinalities_map:
        print "%s hierarchy not found!" % MAIN_HIERARCHY
        return {}

    # for ic we only want the N-1 and N-2 scopes (1 and 2 places lower than national)
    scopes = hierarchies[MAIN_HIERARCHY]
    ic_scopes = scopes[1:-1]    # discard nation and cgi scope (first and last)
    ic_scopes = ic_scopes[:2]   # keep at most the first 2 of the remaining

    if len(ic_scopes) == 0:
        print "No N-1 and N-2 scopes available in %s hierarchy!" % MAIN_HIERARCHY
        return {}

    # assign the cardinality (number of entries for that scope) to each ic_scope
    locations_per_scope_count = hierarchy_cardinalities_map[MAIN_HIERARCHY]
    for s in ic_scopes:
        s["cardinality"] = locations_per_scope_count[s.get("scope_name")]

    if len(ic_scopes) > 0:
        n1_cardinality = ic_scopes[0]["cardinality"]
        if n1_cardinality > IC_CS_MAX_CARDINALITY:
            print "N-1 limit exceeded: %s > %s!" % (n1_cardinality, IC_CS_MAX_CARDINALITY)
            return {}

    if len(ic_scopes) > 1:
        n2_cardinality = ic_scopes[1]["cardinality"]
        if n2_cardinality > IC_CS_MAX_CARDINALITY:
            print "N-2 limit exceeded: %s > %s!" % (n2_cardinality, IC_CS_MAX_CARDINALITY)
            return {}

    # determine the nlive-appropriate name for each ic_scope
    for s in ic_scopes:
        nlive_generic_scopes = {"scope_1", "scope_2", "scope_3", "scope_4"}
        nlive_scope_name = s.get("scope_name")
        for name in nlive_generic_scopes:
            if name in s.get("scopemap_keys", []):
                nlive_scope_name = name
                break
        s["nlive_scope_name"] = nlive_scope_name

    ic_scopes_map = {}
    for i, s in enumerate(ic_scopes):
        ic_scopes_map["interconnect_cs_location_%s" % (i + 1)] = s["nlive_scope_name"]

    return ic_scopes_map


def locations_updater(customer, input_path="data", output_path="artifacts", save_optimization=False):
    label = "***** Starting location update process for: [%s] *****" % customer
    print "\r"
    print "*" * len(label)
    print label
    print "*" * len(label)

    COUNTRIES_JSON_PATH = "%s/global/country_information.json" % (input_path,)
    COUNTRIES_GEOJSON_DIR = "%s/global/geojson/country" % (input_path,)
    CONFIG_PATH = "%s/customers/%s/config/" % (input_path, customer,)
    GEOJSON_PATH = "%s/customers/%s/geojson/" % (input_path, customer,)
    OUTPUT_PATH = "%s/customers/%s/files" % (output_path, customer,)

    # delete artifacts/<CUSTOMER>/files directory and its contents
    if os.path.exists(OUTPUT_PATH):
        shutil.rmtree(OUTPUT_PATH)

    # remake it
    os.makedirs(OUTPUT_PATH)

    def dump_json_to_artifacts(obj, filename):
        obj_filepath = "%s/%s" % (OUTPUT_PATH, filename)
        print "Writing: %s" % filename
        with open(obj_filepath, "w") as fp:
            json.dump(obj, fp, indent=4)

    def dump_txt_to_artifacts(obj, filename):
        obj_filepath = "%s/%s" % (OUTPUT_PATH, filename)
        print "Writing: %s" % filename
        with open(obj_filepath, "w") as fp:
            for row in obj:
                fp.write(row)
                fp.write("\n")

    def copy_file_to_artifacts(filename):
        file_config = "%s/%s" % (CONFIG_PATH, filename)
        if os.path.exists(file_config):
            file_artifact = "%s/%s" % (OUTPUT_PATH, filename)
            shutil.copyfile(file_config, file_artifact)
            return True
        return False

    files_to_copy = [
        # copy pois file
        "pois.json",
        # copy usermap file
        "usermap.json",
        # copy data aggregations related files
        "m2m_whitelist.txt",
        "iot_whitelist.txt",
        "eventlog.json.maps.local",
        "apn-local.json",
        # custom dlg files
        "dlg_config.json",
        # gmrs related file
        "gmrs.video_experience_apps.txt",
        # custom analytics_id files
        "feature.crisis.json",
        "analytics_id_groups.json",
        # copy user aliases (only used in singtel atm)
        "user_aliases.config.json",
        # copy non-scopemap file
        "non-scopemap.json",
        # copy rating rules file
        "rating_rules.json",
        # scp data science related files
        "scp_model.pkl",
        "scp_feature_importance.csv",
        # optional kpi thresholds override file
        "kpi_thresholds_overrides.json",
        # db_expire override
        "db_expire.conf.json",
        # started being bundled with maps on 2020/12/17
        "nodemap.json",
        # gr-mrs copy specs (where available)
        "ingress.spec.json",
        # sni fraud domains (only used in telkomsel atm)
        "snifraud_domains.json",
    ]

    config_reader = ConfigReader(CONFIG_PATH, GEOJSON_PATH, COUNTRIES_JSON_PATH, COUNTRIES_GEOJSON_DIR, save_optimization)

    # prepare locations info
    locations_db_data, ic_scopes_map = _update_locations(customer, config_reader, save_optimization)

    # prepare locations gis data
    try:
        locations_gis_db_data = get_locations_gis(config_reader)
    except Exception as ex:
        print >> sys.stderr, "Error:", ex
        locations_gis_db_data = []

    # All went well. Save results...
    copied_files = []
    for f in files_to_copy:
        found_and_copied = copy_file_to_artifacts(f)
        if found_and_copied:
            copied_files.append(f)

    print "Copied files: %s" % ", ".join(copied_files)

    # save key_apps
    dump_txt_to_artifacts(config_reader.get_key_apps(), "key_apps.txt")

    # save priority_apps
    dump_txt_to_artifacts(config_reader.get_priority_apps(), "priority_apps.txt")

    # save modified poi2cell.json
    dump_json_to_artifacts(config_reader.poi2cell, "poi2cell.json")

    # save scopemap file
    dump_json_to_artifacts(config_reader.scopemap, "raw-scopemap.json")
    dump_json_to_artifacts(config_reader.nlive_aggregations, "nlive_aggregations.json")

    # save locations file for db import
    dump_json_to_artifacts(locations_db_data, "locations.json")
    dump_json_to_artifacts(locations_gis_db_data, "locations_gis.json")

    # save interconnect_cs info for scopes n-1, n-2 (read by nlive)
    dump_json_to_artifacts(ic_scopes_map, "interconnect_cs.json")

    # save scopes.json for nlive and co use
    dump_json_to_artifacts(config_reader.scopes_info, "scopes.json")
    dump_json_to_artifacts(config_reader.get_scopes_active(), "scopes.active.json")

    # save modified cellmap
    dump_json_to_artifacts(config_reader.cellmap_raw, "cellmap.json")

    # save modified cellmap.scopes
    dump_json_to_artifacts(config_reader.get_cellmap_scopes(), "cellmap.scopes.json")

    merged_scopemap = scopemap_merger.scopemap_merger(config_reader.scopemap, config_reader.non_scopemap, config_reader.scopes_info)

    dump_json_to_artifacts(merged_scopemap, "scopemap.json")

    label = "***** Finished location update process for: [%s] *****" % customer
    print "\r"
    print "*" * len(label)
    print label
    print "*" * len(label)

    return config_reader

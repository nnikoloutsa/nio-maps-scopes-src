#!/usr/bin/env python

import logging
import json
import os
import sys
import argparse

JSON = 1
JSON_PER_LINE = 2

ALLOWED_HIERARCHIES = set(["api", "mapview"])

# Note: this list should be updated when nlive adds new harcoded
# dimensions
NLIVE_HARDCODED_DIMENSIONS = [
    "apn",
    "apn_group",
    "app",
    "app_category",
    "app_component",
    "app_server_ip",
    "application",
    "as",
    "bras",
    "bsc",
    "business_area",
    "business_cluster",
    "business_sales_area",
    "cgi",
    "city",
    "cluster",
    "cscf",
    "device",
    "device_category",
    "device_vendor",
    "dns",
    "dns_enum_server",
    "domain",
    "ggsn",
    "hlr",
    "hss",
    "inst_type",
    "institution",
    "ip",
    "ip_group",
    "isp",
    "isup_switch",
    "mme",
    "msc",
    "next_resolution",
    "node_type",
    "ocs",
    "ofcs",
    "operator_group",
    "os",
    "overall",
    "pcrf",
    "pgw",
    "poi",
    "province",
    "radius_server",
    "rat",
    "region",
    "resolution",
    "rnc",
    "roaming_country",
    "roaming_direction",
    "roaming_operator",
    "roaming_plmn",
    "scope_1",
    "scope_2",
    "scope_3",
    "scope_4",
    "scp",
    "segment",
    "service_key",
    "sgsn",
    "sgw",
    "site",
    "smsc",
    "tai",
    "vendor",
    "vlr",
    "wag",
]

NLIVE_NAILED_ABSTRACT_SCOPES = [
    "scope_1",
    "scope_2",
    "scope_3",
    "scope_4"
]

NLIVE_NAILED_NAMED_SCOPES = [
    "city",
    "region",
    "business_area",
    "bussiness_sales_area",
    "business_cluster",

    "province",
    "cluster",
]

NLIVE_ITS_COMPLICATED_ALMOST_SCOPES = [
    "site"
]

API_NAILED_SCOPES = [
    "city",
    "region",
    "business_area",
    "bussiness_sales_area",
    "business_cluster",

    "district",
]


class Colorizer(object):

    WHITE = "\033[1;37m"
    RED = "\033[1;31m"
    BLUE = "\033[1;34m"
    GREEN = "\033[1;32m"
    YELLOW = "\033[1;33m"
    PURPLE = "\033[1;35m"

    DEBUG = "DEBUG"
    INFO = "INFO"
    WARNING = "WARNING"
    CRITICAL = "CRITICAL"
    ERROR = "ERROR"

    def __init__(self, dont_check_tty=False):

        if dont_check_tty:
            self.IS_TTY = False
        else:
            self.IS_TTY = sys.stdout.isatty()

        self.levelToColorMap = {
            "DEBUG": Colorizer.BLUE,
            "INFO": Colorizer.GREEN,
            "WARNING": Colorizer.YELLOW,
            "ERROR": Colorizer.RED,
            "CRITICAL": Colorizer.PURPLE,
        }

        self.nameToColorMap = {
            "WHITE": Colorizer.WHITE,
            "BLUE": Colorizer.BLUE,
            "GREEN": Colorizer.GREEN,
            "YELLOW": Colorizer.YELLOW,
            "PURPLE": Colorizer.PURPLE,
            "RED": Colorizer.RED,
        }
        self.color_end = "\033[0m"

    def colorize(self, text, level=None, color=None):
        if not self.IS_TTY:
            return text

        if not level and not color:
            return text

        if level and level in self.levelToColorMap:
            color_start = self.levelToColorMap[level]
            text = "%s%s%s" % (color_start, text, self.color_end)
        elif color and color in self.nameToColorMap:
            color_start = self.nameToColorMap[color]
            text = "%s%s%s" % (color_start, text, self.color_end)

        return text


class ConfigurationValidator(object):

    def __init__(self, customer):
        self.customer = customer
        self.colorizer = Colorizer()

        self.nlive_aggregations = self.read_config("nlive_aggregations.json")
        self.scopes = self.read_config("scopes.json")
        self.eventlog = self.read_config("eventlog.json.maps.local", kind=JSON_PER_LINE)
        self.analytics_id_groups = self.read_config("analytics_id_groups.json")

    def print_header(self, message):
        self.log_header("#" * 50)
        self.log_header("#### %s" % message)
        self.log_header("#" * 50)
        self.log_header("-" * 50)

    def log_error(self, message):
        message = self.colorizer.colorize(message, level="ERROR")
        logging.error(message)

    def log_info(self, message):
        message = self.colorizer.colorize(message, level="INFO")
        logging.info(message)

    def log_header(self, message):
        logging.info(message)

    def read_config(self, filename, default=None, kind=JSON):
        try:

            with open("./data/customers/%s/config/%s" % (self.customer, filename)) as fp:
                contents = fp.read()
                contents = contents.strip()

                if kind == JSON_PER_LINE:
                    last_comma_pos = contents.rfind(",")
                    contents = "[" + contents[:last_comma_pos] + "]"

                return json.loads(contents)

        except Exception as e:
            self.log_error("Could not read %s: %s" % (filename, e))
            return default

    def check_scopes_json(self):
        self.print_header("CHECKING SCOPES.JSON FILE")

        if not self.scopes:
            self.log_info("Empty/missing/malformed scopes file!")
            return

        scope_names = [cur_scope.get("scope_name") for cur_scope in self.scopes["scopes"]]

        hierarchies = self.scopes.get("hierarchies", [])

        for h, h_scopes in hierarchies.items():
            if h not in ALLOWED_HIERARCHIES:
                self.log_error("hierarchy: %s is not known!" % h)

            for s in h_scopes:
                if not type(s) is dict:
                    # myren has some
                    # include_root, include_leaf entries
                    # skip them
                    continue
                if s.get("scope_name") not in scope_names:
                    self.log_error("scope_name: %s used in hierarchy: %s is not defined in scopes!" % (s.get("scope_name"), h))

                if s.get("scope_name") == "site":
                    site_scope_spec = {}
                    for cur_scope in self.scopes["scopes"]:
                        if cur_scope.get("scope_name") == "site":
                            site_scope_spec = cur_scope
                            break

                    if not site_scope_spec or ("site" not in site_scope_spec.get("scopemap_keys") and "bts" not in site_scope_spec.get("scopemap_keys")):
                        self.log_error("Scope 'site' or 'bts' must be in listed in scopes.json scopemap_keys for live aggregations to work for site/bts")

    def check_eventlog(self):
        self.print_header("CHECKING EVENTLOG FILE")

        if not self.eventlog:
            self.log_info("Empty/missing/malformed eventlog file!")
            return

        for spec in self.eventlog:

            module = spec.get("module")

            self.log_header("###### Checking: %s" % module)

            logfile = spec.get("logfile")

            # "module":  "live.scope_4.rat" -> ["live", "scope_4", "rat"]
            module_tokens = module.split(".")

            try:
                assert module_tokens[0] in ["live", "live_no_aggr"]
            except Exception:
                self.log_error("module %s should be either 'live' or 'live_no_aggr'" % module_tokens[0])

            # ["live", "scope_4", "rat"]  -> ["scope_4", "rat"]
            module_dimensions = module_tokens[1:]

            # "logfile":  "b_micro_cluster.rat", ---> ["b_micro_cluster", "rat"]
            logfile_dimensions = logfile.split(".")

            # check that module list the same number of dimensions as logfile
            if len(module_dimensions) != len(logfile_dimensions):
                self.log_error("Module dimensions list: %s" % ", ".join(module_dimensions))
                self.log_error("!== logfile dimensions: %s" % ", ".join(logfile_dimensions))

            # check that logfile doesn't mention any abstract scope
            if "scope_" in logfile:
                self.log_error("Logfile references abstract scope: %s" % logfile)

            # from the dimensions listed, find those reffering to abstract location scopes
            # ["scope_4", "rat"] -> ["scope_4"]
            abstract_scopes_in_module = [md for md in module_dimensions if md.startswith("scope_")]
            # allow exactly 1
            if len(abstract_scopes_in_module) == 0:
                self.log_error("No location scopes in module dimensions: %s" % ", ".join(module_dimensions))
            elif len(abstract_scopes_in_module) > 1:
                self.log_error("More than 1 location scopes in module dimensions: %s" % ", ".join(abstract_scopes_in_module))
            else:
                # check that all scope module dimensions are declared in scopes.json
                # ["scope_4"] -> "scope_4"
                module_abstract_scope = abstract_scopes_in_module[0]

                scopes_json_abstract_scopes = [cur_scope.get("analytics_key") for cur_scope in self.scopes["scopes"]]
                scopes_json_named_scopes = [cur_scope.get("scope_name") for cur_scope in self.scopes["scopes"]]

                if module_abstract_scope not in scopes_json_abstract_scopes:
                    self.log_error("Module refers to scope not found in scopes.json: %s" % module_abstract_scope)

                # find mapped named scope for abstract scope
                # e.g. ["scope_4", "rat"] and ["b_micro_cluster", "rat"] means "scope_4" is mapped to "b_micro_cluster"
                logfile_named_scope_set = set(logfile_dimensions) - (set(module_dimensions) & set(logfile_dimensions))

                # check that eventlog only maps scopes not hardcoded in nlive as named scopes
                # (because the latter don't need eventlog to be created)
                if len(logfile_named_scope_set) == 0:
                    self.log_error("%s does not match to named scope!" % module_abstract_scope)
                elif len(logfile_named_scope_set) > 1:
                    self.log_error("Multiple matching scopes for %s!" % module_abstract_scope)
                elif len(logfile_named_scope_set) == 1:
                    logfile_named_scope = list(logfile_named_scope_set)[0]

                    if logfile_named_scope in (NLIVE_NAILED_ABSTRACT_SCOPES + NLIVE_NAILED_NAMED_SCOPES):
                        self.log_error("Eventlog matches %s --> %s, but %s is one of nlive hardcoded scopes: %s" % (module_abstract_scope, logfile_named_scope, logfile_named_scope))

                    if logfile_named_scope not in scopes_json_named_scopes:
                        self.log_error("Eventlog assigns: %s to: %s logfile but %s not in scopes.json" % (
                            module_abstract_scope,
                            logfile_named_scope,
                            logfile_named_scope
                        ))

                if logfile_named_scope in NLIVE_ITS_COMPLICATED_ALMOST_SCOPES:
                    self.log_error("Scopes: '%s' should not be listed in eventlog conf" % ", ".join(NLIVE_ITS_COMPLICATED_ALMOST_SCOPES))

            # check that only abstract scopes listed in scopes.json are refferenced to
            # in the eventlog.json.local
            scopes_json_named_scopes = [cur_scope.get("scope_name") for cur_scope in self.scopes["scopes"]]

            if len(module_dimensions) > 0:
                named_scopes_in_module = set(module_dimensions) & set(scopes_json_named_scopes)

                if len(named_scopes_in_module) > 0:
                    self.log_error("Eventlog entry contains non-abstract scopes: %s" % ", ".join(named_scopes_in_module))

            print "-" * 50

    def check_nlive_aggregations(self):
        if not self.nlive_aggregations:
            logging.warning("Empty/missing/malformed nlive_aggregations file!")
            return

        self.print_header("CHECKING NLIVE AGGREGATIONS FILE")

        for aggr_id, aggr_spec in self.nlive_aggregations.iteritems():

            enabled = aggr_spec.get("enabled", True)

            self.log_header("###### Checking: %s" % aggr_id)

            if not enabled:
                self.log_info("###### %s is not enabled" % aggr_id)
            else:
                aggr_id_dimensions = aggr_id.split(".")

                for dim in aggr_id_dimensions:
                    if dim not in NLIVE_HARDCODED_DIMENSIONS:
                        # any aggrgeation dimension must be known by nlive
                        self.log_error("##### Non nlive-known dimension in nlive_aggregations: %s" % dim)

                    if dim.startswith("scope_") and dim not in (NLIVE_NAILED_ABSTRACT_SCOPES + NLIVE_NAILED_NAMED_SCOPES):
                        # nlive_aggregations can only use one of the hardcoded nlive scope names
                        self.log_error("##### Non nailed scope (!= scope_1 to _4) in nlive_aggregations: %s" % dim)

                # check that filter dimensions and aggregations match
                filter_aggregations = set(aggr_spec.get("filters", {}).get("aggregate", []))

                # adding application as a special case, needed for nlive
                if not filter_aggregations.issubset(set(aggr_id_dimensions) | set(["application"])):
                    self.log_error("###### Filter aggregation dimensions: %s" % ", ".join(sorted(filter_aggregations)))
                    self.log_error("######  are not subset of dimensions: %s" % ", ".join(sorted(aggr_id_dimensions)))

            print "-" * 100


if __name__ == "__main__":

    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument("--customer", type=str, required=False, help="Customer id")
    parser.add_argument("--all", action="store_true", required=False, help="Check all customers")

    args = parser.parse_args()

    CUSTOMERS = {f for f in os.listdir("./data/customers") if not f.startswith(".")}

    if not args.all and not args.customer:
        parser.print_help()
        sys.exit(1)

    if args.customer:
        if args.customer not in CUSTOMERS:
            print "Unknown customer specified: %s" % args.customer
            sys.exit(1)

        cv = ConfigurationValidator(customer=args.customer)
        cv.check_scopes_json()
        cv.check_nlive_aggregations()
        cv.check_eventlog()

    elif args.all:
        for customer in CUSTOMERS:
            print "*" * 50
            print "*** CUSTOMER: %s" % customer
            print "*" * 50
            cv = ConfigurationValidator(customer=customer)
            cv.check_scopes_json()
            cv.check_nlive_aggregations()
            cv.check_eventlog()
